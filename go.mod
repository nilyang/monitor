module gitee.com/nilyang/monitor

go 1.16

require (
	github.com/astaxie/beego v1.12.3
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-ini/ini v1.62.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/websocket v1.4.2
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/howeyc/fsnotify v0.9.0
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/pkg/errors v0.9.1
	github.com/robbert229/jwt v2.0.0+incompatible
	github.com/smartystreets/goconvey v1.6.4
	github.com/streadway/amqp v1.0.0
	github.com/tidwall/gjson v1.8.0
	github.com/twpayne/go-kml v1.5.2
	gopkg.in/ini.v1 v1.62.0 // indirect
)

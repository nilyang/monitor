package structs

import (
	"fmt"
	"strings"
	"testing"
)

func TestCloudStorage(t *testing.T) {
	fid := "7,7916fa2e2e"
	url := strings.Join(strings.Split(fid, ","), "/") + ".jpg"
	fmt.Printf("fidUrl=%s\n", url)
}

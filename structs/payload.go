package structs

import (
	"fmt"
	"strings"
)

// Jwt payload
type Payload map[string]interface{}

// save https://github.com/chrislusf/seaweedfs storage info
type StorageFile struct {
	Fid       string `json:"fid"`
	Url       string `json:"url"`
	PublicUrl string `json:"publicUrl"`
	Count     int64  `json:"count"`
	Size      int64  `json:"size"`
}

// fid=7,7916fa2e2e --> 7/7916fa2e2e.jpg
func FormatCloudPathByFid(fid string) string {
	return strings.Join(strings.Split(fid, ","), "/") + ".jpg"
}

type Alarm struct {
	Id               int64    `json:"id"`
	Name             string   `json:"name"`
	StationId        string   `json:"stationId"`
	StationLongitude float64  `json:"stationLongitude"`
	StationLatitude  float64  `json:"stationLatitude"`
	FireLongitude    float64  `json:"fireLongitude"`
	FireLatitude     float64  `json:"fireLatitude"`
	AlarmTime        string   `json:"alarmTime"`
	Images           []string `json:"images"`
}

type Position struct {
	Id        string  `json:"id"`        // 记录编号
	Name      string  `json:"name"`      // 使用者姓名
	Type      int     `json:"type"`      // 1 ren 2 car
	GpsNumber string  `json:"gpsNumber"` // GPS 设备编号
	Longitude float64 `json:"longitude"` // 经度
	Latitude  float64 `json:"latitude"`  // 纬度
	Altitude  float64 `json:"altitude"`  // 高度
	WriteTime string  `json:"writeTime"` // 位置记录时间
}

type PositionView struct {
	Id        string `json:"id"`        // 记录编号
	Name      string `json:"name"`      // 使用者姓名
	Type      int    `json:"type"`      // 1 ren 2 car
	GpsNumber string `json:"gpsNumber"` // GPS 设备编号
	Longitude string `json:"longitude"` // 经度
	Latitude  string `json:"latitude"`  // 纬度
	Altitude  string `json:"altitude"`  // 海拔
	WriteTime string `json:"writeTime"` // 位置记录时间
}

func (p PositionView) ToKmlPointString() string {
	return fmt.Sprintf("%16s,%16s,%s", p.Longitude, p.Latitude, p.Altitude)
}

///{{{ 发消息到APP
type MessagesBody struct {
	Type    int      `json:"type"`    // 消息类型
	Content string   `json:"content"` // 消息内容
	Images  []string `json:"images"`  // 关联图片地址
}

type MessagesResponse struct {
	Id string `json:"id"` // 消息ID
	MessagesBody
}

type MessageCheckResult struct {
	Id       string `json:"id"`       // 消息ID
	Status   int    `json:"status"`   // 消息状态 1 已读 2 未读
	ReadTime string `json:"readTime"` // 消息阅读时间
}

///}}}

////////////////////////////////
//  消息体内容
////////////////////////////////

type MsgToken struct {
	JsonData string // 原始信息 JSON
	Message  string
	Type     string // zai|ren|che
	Source   string // moniter|watcher
	Code     string
	Name     string
	Time     string // refresh|occur time
	Location string // 点|矩形对角位置...等...
	Point    string // 点
	PicUrl   string
	Hash     string
}

func (mt MsgToken) fromType() string {
	msg := strings.Split(mt.Message, "|")
	return msg[0]
}

func (mt MsgToken) IsZai() bool {
	return "zai" == mt.fromType()
}

func (mt MsgToken) IsRen() bool {
	return "ren" == mt.fromType()
}
func (mt MsgToken) IsChe() bool {
	return "che" == mt.fromType()
}

func (mt *MsgToken) ParseToken() bool {
	msg := strings.Split(mt.Message, "|")

	//libs.TaskNew(libs.Task{Code: msg[0], Location:msg[1], Status:0})
	//  zai|moniter(or watcher)
	//  code, name, location,
	//  imgId, imgUrl, imgHash,
	//	alarmJson
	mt.Type = msg[0]     // zai|ren|che
	mt.Source = msg[1]   // moniter | watcher
	mt.Code = msg[2]     // device code
	mt.Name = msg[3]     // name
	mt.Time = msg[4]     // time
	mt.Point = msg[5]    // alarm location
	mt.Location = msg[6] // alarm location
	mt.PicUrl = msg[7]   // alarm pic url
	mt.Hash = msg[8]     // pic file hash
	mt.JsonData = msg[9] // origin(or extra)json data

	return true
}

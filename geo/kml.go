package geo

import (
	"encoding/xml"
	"log"
	"strconv"

	"gitee.com/nilyang/monitor/utils"
	"github.com/twpayne/go-kml"
)

// CharData 是cdata类型
type CharData string

// MarshalXML 格式化为xml字段，主要是处理CDATA
func (n CharData) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(struct {
		S string `xml:",cdata"`
	}{
		S: "<![CDATA[" + string(n) + "]]>",
	}, start)
}

// FireKml p1 左上角 p2 右下角
func FireKml(p1, p2 utils.Point) string {
	north := p1.Lat
	south := p2.Lat
	east := p2.Lng
	west := p1.Lng
	routation := 0.4501188911473431

	//var str CharData = `火情发生，请注意！<img src="http://earth.demo:8080/static/earth/6d6.png">`
	//desc, _ := xml.MarshalIndent(str, "", " ")
	k := kml.KML(
		kml.GroundOverlay(
			kml.Name("灾情"),
			//kml.Description("####"),
			kml.NetworkLink(
				kml.RefreshVisibility(false),
				kml.RefreshInterval(3),
				kml.Icon(
					kml.Href("http://earth.demo:8080/static/earth/6d6.png"),
					kml.ViewBoundScale(0.75),
				),
				kml.Visibility(true),
			),
			kml.LatLonBox(
				kml.North(north),
				kml.South(south),
				kml.East(east),
				kml.West(west),
				kml.Rotation(routation),
			),
		),
	)

	//if err := k.WriteIndent(os.Stdout, "", " "); err != nil {
	//    log.Fatal(err)
	//}

	got, err := xml.Marshal(k)
	if err != nil {
		log.Fatal(err)
	}

	return string(got)
}

// RenKml 人kml
func RenKml(p utils.Point) string {
	iconURL := "http://earth.demo:8080/static/earth/ren.png"
	k := kml.KML(
		kml.Document(
			kml.Name("duiwu.kml"),
			kml.SharedStyle(
				"failed10",
				kml.IconStyle(
					kml.Scale(1.4),
					kml.Icon(
						kml.Href(iconURL),
					),
					kml.HotSpot(
						kml.Vec2{X: 0.5, Y: 0.5, XUnits: "fraction", YUnits: "fraction"},
					),
				),
				kml.ListStyle(),
			),
			kml.SharedStyle(
				"failed00",
				kml.IconStyle(
					kml.Scale(1.2),
					kml.Icon(
						kml.Href(iconURL),
					),
					kml.HotSpot(
						kml.Vec2{X: 0.5, Y: 0.5, XUnits: "fraction", YUnits: "fraction"},
					),
				),
				kml.ListStyle(),
			),
			kml.SharedStyleMap(
				"failed2",
				kml.Pair(
					kml.Key("normal"),
					kml.StyleURL("#failed00"),
				),
				kml.Pair(
					kml.Key("highlight"),
					kml.StyleURL("#failed10"),
				),
			),
			kml.Placemark(
				kml.Name("duiwu"),
				kml.Description("<![CDATA[<TABLE BORDER=1></TABLE>]]>"),
				kml.StyleURL("#failed2"),
				kml.Point(
					kml.Coordinates(kml.Coordinate{Lat: p.Lat, Lon: p.Lng, Alt: p.Alt}),
				),
			),
		),
	)

	got, err := xml.Marshal(k)
	if err != nil {
		log.Fatal(err)
	}

	return string(got)
}

// CheKml 车kml生成工具
func CheKml(px []utils.Point) string {
	iconURL := "http://earth.demo:8080/static/earth/che.png"
	k := kml.KML(nil)
	doc := kml.Document(
		kml.SharedStyle(
			"Pt_STYLE",
			kml.IconStyle(
				kml.Scale(2.5),
				kml.Icon(
					kml.Href(iconURL),
				),
				kml.HotSpot(kml.Vec2{X: 20, Y: 2, XUnits: "pixels", YUnits: "pixels"}),
			),
		),
	)

	for idx, p := range px {
		name := "救援车" + strconv.Itoa(idx+1)
		doc.Add(
			kml.Placemark(
				kml.Name(name),
				kml.Description(name),
				kml.StyleURL("#Pt_STYLE"),
				kml.Point(
					kml.Coordinates(kml.Coordinate{Lat: p.Lat, Lon: p.Lng, Alt: p.Alt}),
				),
			),
		)
	}

	k.Add(doc)

	got, err := xml.Marshal(k)
	if err != nil {
		log.Fatal(err)
	}

	return string(got)

}

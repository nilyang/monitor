package libs

import (
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"
)

///{{{
type MyPath string

func (mp MyPath) ToString() string {
	return string(mp)
}

func (mp MyPath) Name() string {
	name := filepath.Base(mp.ToString())
	return name
}

func (mp MyPath) Ext() string {
	path := string(mp)
	name := filepath.Base(path)
	ext := filepath.Ext(name)
	return strings.ToLower(strings.TrimLeft(ext, "."))
}

func (mp MyPath) PureName() string {
	name := filepath.Base(mp.ToString())
	return strings.Replace(name, filepath.Ext(name), "", 1)
}

func (mp MyPath) IsPng() bool {
	return mp.Ext() == "png"
}

func (mp MyPath) IsTxt() bool {
	return mp.Ext() == "txt"
}

///}}}
func CopyFile(src string, dest string) (bool, error) {
	if src == "" || dest == "" {
		return false, errors.New("源地址或者目标地址为空！")
	}

	if _, err := os.Stat(src); os.IsNotExist(err) {
		return false, errors.New("源文件不存在")
	}

	// 打开文件资源
	src_fd, err := os.Open(src)
	if err != nil {
		return false, err
	}

	defer src_fd.Close()

	// 打开目标文件
	dest_fd, err := os.OpenFile(dest, os.O_CREATE|os.O_RDWR, 644)
	if err != nil {
		return false, err
	}
	defer dest_fd.Close()

	// 拷贝数据
	_, copy_err := io.Copy(dest_fd, src_fd)
	if copy_err != nil {
		return false, err
	}

	return true, nil
}

func RemoveFile(file_path string) (bool, error) {

	if _, err := os.Stat(file_path); os.IsNotExist(err) {
		return false, errors.New("文件不存在")
	}

	err := os.Remove(file_path)
	if err != nil {
		return false, errors.New("移除文件操作失败")
	}

	return true, nil
}

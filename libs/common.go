package libs

import (
	"encoding/json"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"reflect"
	"runtime"
	"time"
)

const PATH_SEPERRATOR string = string(os.PathSeparator)
const FMT_YYYYMMDD_hhmmss_T string = "2006-01-02T15:04:05"
const FMT_YYYYMMDD_hhmmss string = "2006-01-02 15:04:05"

type WatchDir struct {
	Dir  string
	Type string
}

type ConfigStruct struct {
	Sleeptime int
	Mqdsn     string
	SrvApi    string
	Fileapi   string
	CloudHost string
	Msgapi    string
	Demo      struct {
		IsDemo    bool
		BaseDir   string
		AlarmFile string
		AlarmPic  string
		RenFile   string
		CheFile   string
	}
	WatchDirs []WatchDir
}

type WatchMap struct {
	WatchDir WatchDir
	Watch    fsnotify.Watcher
}

func (wd WatchDir) IsZai() bool {
	return wd.Type == "zai"
}
func (wd WatchDir) IsChe() bool {
	return wd.Type == "che"
}
func (wd WatchDir) IsRen() bool {
	return wd.Type == "ren"
}

func ReadConfig(config_file string) ConfigStruct {

	if IsEmpty(config_file) {
		config_file = "./config.json"
	}

	file, _ := os.Open(config_file)
	defer file.Close()

	decoder := json.NewDecoder(file)
	conf := ConfigStruct{}
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("配置文件错误，即将退出程序: ", err)
		time.Sleep(time.Second * 5)
		runtime.Goexit()
	}

	return conf
}

func IsEmpty(params interface{}) bool {
	var (
		flag          bool = true
		default_value reflect.Value
	)

	r := reflect.ValueOf(params)

	// 获取对应类型默认值
	default_value = reflect.Zero(r.Type())
	// 由于params 接口类型 所以default_value也要获取对应接口类型的值
	// 如果获取不为接口类型 一直为返回false
	if !reflect.DeepEqual(r.Interface(), default_value.Interface()) {
		flag = false
	}
	return flag
}

func FileGetContents(filename string) ([]byte, error) {
	const TRY_TIMES = 1000
	var (
		err     error
		content []byte
		i       int = 0
	)

	for {
		//log.Printf("ReadFile [%d] times\n",i)
		content, err = ioutil.ReadFile(filename)

		if os.IsNotExist(err) {
			//log.Println("Not Exists")
			return nil, err
		}

		if os.IsPermission(err) {
			//fmt.Println(err)
			// 等待以获取读取权限
			time.Sleep(1 * time.Second)
			// 尝试次数
			if i > TRY_TIMES {
				return nil, errors.New("No permission ,try max times :" + string(TRY_TIMES))
			}
			i++
			continue
		}

		if err != nil {
			//log.Println("Other error")
			time.Sleep(10 * time.Millisecond)
			// 尝试次数
			if i > TRY_TIMES {
				return nil, err
			}
			i++
			continue
		}

		break
	}

	return content, err
}

// 2006-01-02T15:04:05 --> 2006-01-02 15:04:05
func TimeFormatFromT(t string) (time.Time, error) {
	tt, err := time.Parse(FMT_YYYYMMDD_hhmmss_T, t)
	if err != nil {
		return time.Time{}, err
	}
	return tt, nil
}

// 2006-01-02 15:04:05 --> 2006-01-02T15:04:05
func TimeFormatToT(t string) (string, error) {
	tt, err := time.Parse(FMT_YYYYMMDD_hhmmss, t)
	if err != nil {
		return "", nil
	}
	return tt.Format(FMT_YYYYMMDD_hhmmss_T), nil
}

package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AdminUserController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:AlarmController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:FileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:FileController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:FileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:FileController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:FileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:FileController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:FileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:FileController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:FileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:FileController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GpsReportController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GroupController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GroupController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GroupController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GroupController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GroupController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GroupController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GroupController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GroupController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:GroupController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:GroupController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:MessageController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:MessageController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:MessageController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:MessageController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:MessageController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:MessageController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:MessageController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:MessageController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:MessageController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:MessageController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"],
		beego.ControllerComments{
			Method: "GetOne",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:ProfileController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "PostInit",
			Router: `/init`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["monitor/server/controllers:UserController"] = append(beego.GlobalControllerRouter["monitor/server/controllers:UserController"],
		beego.ControllerComments{
			Method: "Logout",
			Router: `/logout`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}

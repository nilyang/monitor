// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"gitee.com/nilyang/monitor/server/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/plugins/cors"

	//"log"
	"gitee.com/nilyang/monitor/utils"
)

var URL_MAP map[string]bool = make(map[string]bool)

func ShouldCheckUrlAuth(url string) bool {
	URL_MAP = map[string]bool{
		"/v1/user/login":  true,
		"/v1/user/logout": true,
	}

	_, ok := URL_MAP[url]
	if !ok {
		return true
	}
	return false
}

func init() {
	// CORS
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"X-Token", "Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
	}))
	// 首页
	beego.Router("/", &controllers.ObjectController{}, "get:Index")

	ns := beego.NewNamespace("/v1",
		beego.NSCond(func(ctx *context.Context) bool {
			xtoken := ctx.Input.Header("X-Token")
			ok := ShouldCheckUrlAuth(ctx.Input.URL())
			//log.Println("ok:", ok, "\ntoken-validate:", utils.JwtValidate(utils.SECRET_KEY, xtoken))
			if ok {
				//return true //TODO remove comment
				if xtoken == "" {
					return false
				} else if !utils.JwtValidate(utils.SECRET_KEY, xtoken) {
					return false
				}
			}
			return true
		}),
		beego.NSNamespace("/object",
			beego.NSInclude(
				&controllers.ObjectController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
		beego.NSNamespace("/alarm",
			beego.NSInclude(
				&controllers.AlarmController{},
			),
		),
		beego.NSNamespace("/report",
			beego.NSInclude(
				&controllers.GpsReportController{},
			),
		),
		beego.NSNamespace("/device",
			beego.NSInclude(
				&controllers.DeviceController{},
			),
		),
		beego.NSNamespace("/profile",
			beego.NSInclude(
				&controllers.ProfileController{},
			),
		),
		beego.NSNamespace("/group",
			beego.NSInclude(
				&controllers.GroupController{},
			),
		),
		beego.NSNamespace("/file",
			beego.NSInclude(
				&controllers.FileController{},
			),
		),
		beego.NSNamespace("/message",
			beego.NSInclude(
				&controllers.MessageController{},
			),
			beego.NSRouter("/ws", &controllers.MessageController{}, "get:WsMsg"),
			beego.NSRouter("/send", &controllers.MessageController{}, "post:WsSendMsg"),
		),
	)
	beego.AddNamespace(ns)

	// mock
	beego.Router("/api/v1/realtime/mock", &controllers.MockController{}, "get:StartMock")
	beego.Router("/api/v1/BaseStation/alarm/current", &controllers.MockController{}, "get:Alarm")
	beego.Router("/api/v1/position/realtime", &controllers.MockController{}, "get:Position")
	beego.Router("/api/v1/message/send", &controllers.MockController{}, "get:MessageSend")
	beego.Router("/api/v1/message/check", &controllers.MockController{}, "get:MessageCheck")

	beego.Router("/api/v1/kml", &controllers.MockController{}, "get:GetKml")
	beego.Router("/play", &controllers.MockController{}, "get:Play")

}

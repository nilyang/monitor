<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>监控视频</title>
    <link href="/static/video/video-js.css" rel="stylesheet">

    <script>window.HELP_IMPROVE_VIDEOJS=false</script>

    <!-- If you'd like to support IE8 -->
    <script src="/static/video/videojs-ie8.min.js"></script>

</head>
<body>
<video id="my-video" class="video-js" controls preload="auto" width="1280" height="528"
       poster="/static/bg/earth2.jpg" data-setup="{}">
    <source src="/static/video/oceans.mp4" type='video/mp4'>
    <source src="/static/video/Penguins_of_Madagascar.m4v" type='video/mp4'>
    <p class="vjs-no-js">
        浏览器不支持JS，请在支持js的浏览器中打开
    </p>
</video>

<script src="/static/video/video.js"></script>
</body>

</body>
</html>
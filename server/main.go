//go:generate goversioninfo -icon=logo.ico
package main

import (
	_ "gitee.com/nilyang/monitor/server/routers"

	"net/http"
	"strings"

	"gitee.com/nilyang/monitor/server/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

func init() {
	models.OrmInit()
}

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	// 静态资源目录
	beego.SetStaticPath("/static", "static")
	// 特殊文件处理
	beego.InsertFilter("/favicon.ico", beego.BeforeRouter, TransparentStatic)

	beego.Run()
}

func TransparentStatic(ctx *context.Context) {
	if strings.Index(ctx.Request.URL.Path, "v1/") >= 0 {
		return
	}
	http.ServeFile(ctx.ResponseWriter, ctx.Request, "static/favicon.ico")
}

package controllers

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"gitee.com/nilyang/monitor/server/models"
	"gitee.com/nilyang/monitor/utils"

	"github.com/astaxie/beego"
)

//  AlarmController operations for Alarm
type AlarmController struct {
	beego.Controller
}

// URLMapping ...
func (c *AlarmController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description create Alarm
// @Param	body		body 	models.Alarm	true		"body for Alarm content"
// @Success 201 {int} models.Alarm
// @Failure 403 body is empty
// @router / [post]
func (c *AlarmController) Post() {
	var v models.Alarm
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)

	// 暂时不记录警报历史
	// 只记录设备最后一次警报信息

	// 新增or更新位置数据
	if isUpdate, err := models.InsertOrUpdateAlarmByCode(&v); err != nil {
		c.Data["json"] = utils.JsonError(err)
	} else {
		if isUpdate {
			c.Data["json"] = utils.JsonSuccess("Update OK")
		} else {
			c.Data["json"] = utils.JsonSuccess("Insert OK")
		}
	}

	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get Alarm by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Alarm
// @Failure 403 :id is empty
// @router /:id [get]
func (c *AlarmController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v, err := models.GetAlarmById(id)
	if err != nil {
		c.Data["json"] = utils.JsonError(err.Error())
	} else {
		vv := make(map[string]interface{})
		vv["Id"] = v.Id
		vv["Code"] = v.Code
		vv["Point"] = v.Point
		vv["Time"] = v.Time
		vv["Location"] = v.Location
		c.Data["json"] = utils.JsonSuccess(vv)
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get Alarm
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Alarm
// @Failure 403
// @router / [get]
func (c *AlarmController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	fields = strings.Split("Id,Type,Source,Code,Name,Time,Point,Location,PicUrl,PicHash,Status", ",")
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	//log.Println(fields)
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = utils.JsonError(errors.New("Error: invalid query key/value pair").Error())
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	tasks, err := models.GetAllAlarm(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = utils.JsonError(err.Error())
	} else {
		c.Data["json"] = utils.JsonSuccess(tasks)
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the Alarm
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Alarm	true		"body for Alarm content"
// @Success 200 {object} models.Alarm
// @Failure 403 :id is not int
// @router /:id [put]
func (c *AlarmController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v := models.Alarm{Id: id}
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if err := models.UpdateAlarmById(&v); err == nil {
		c.Data["json"] = utils.JsonSuccess("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}

	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the Alarm
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *AlarmController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if err := models.DeleteAlarm(id); err == nil {
		c.Data["json"] = utils.JsonError("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}
	c.ServeJSON()
}

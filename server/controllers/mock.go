package controllers

import (
	"encoding/json"

	"gitee.com/nilyang/monitor/server/mockutils"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"
	"github.com/astaxie/beego"
)

type MockController struct {
	beego.Controller
}

// http://IP:Port/api/v1/realtime/mock
func (o *MockController) StartMock() {
	if !checkMockStartPositions() {
		o.Data["json"] = utils.JsonError("模拟数据已经开始运行，请等待")
	} else {
		o.Data["json"] = utils.JsonSuccess("模拟数据成功运行")
	}

	o.ServeJSON()
}

// http://IP:Port/api/v1/BaseStation/alarm/current
func (o *MockController) Alarm() {
	checkMockStartAlarm()
	urlHost := "http://" + o.Ctx.Request.Host
	o.Data["json"] = mockutils.GetMockFireData(urlHost)
	o.ServeJSON()
}

// api/v1/position/realtime
func (o *MockController) Position() {
	checkMockStartPositions()
	o.Data["json"] = mockutils.GetMockPositionViews(0)
	o.ServeJSON()
}

// api/v1/message
func (o *MockController) MessageSend() {
	json_response := `
{
"id": "e407f4a47fcd472391b19a3a1dcec2bf",
"type": 1,
"content": "G0-132",
"images": [
"http://192.168.1.103:8088/message01.jpg",
"http://192.168.1.103:8088/message02.jpg"]
}`
	var (
		messagesResponse structs.MessagesResponse
		messagesBody     structs.MessagesBody
	)
	json.Unmarshal(o.Ctx.Input.RequestBody, &messagesBody)

	json.Unmarshal([]byte(json_response), &messagesResponse)
	o.Data["json"] = map[string]interface{}{
		"messagesBody":     messagesBody,
		"messagesResponse": messagesResponse,
	}

	o.ServeJSON()
}

// api/v1/message/{msg_id}
func (o *MockController) MessageCheck() {
	//
	json_str := `
{
"id": "e407f4a47fcd472391b19a3a1dcec2bf",
"status": 1,
"readTime": "2017-11-09T16:05:24"
}`
	var checkResult structs.MessageCheckResult
	json.Unmarshal([]byte(json_str), &checkResult)

	o.Data["json"] = checkResult
	o.ServeJSON()
}

func (o *MockController) GetKml() {
	bizType := o.GetString("type", "")
	addr := "http://" + o.Ctx.Request.Host

	var out []byte
	switch bizType {
	case "huo":
		out = mockutils.RenderHuo(addr)
	case "ren":
		out = mockutils.RenderRen(addr)
	case "che":
		out = mockutils.RenderChe(addr)
	}

	o.OutXML(out)
}

func (o *MockController) OutXML(out []byte) {
	if o.GetString("debug") == "1" {
		o.Ctx.Output.Header("Content-Type", "application/xml; charset=utf-8")
	} else {
		o.Ctx.Output.Header("Content-Type", "application/vnd.google-earth.kml+xml; charset=utf-8")
	}

	o.Ctx.Output.Body(out)
}

func (o *MockController) Play() {
	o.EnableRender = true
	o.Ctx.Output.Header("Content-Type", "text/html; charset=utf-8")
	o.EnableRender = true
	o.TplName = "play.tpl"
	o.Render()
}

func getMockConfig() mockutils.MockConfig {
	conf := mockutils.MockConfig{
		CheMockInterval: beego.AppConfig.DefaultInt("CheMockInterval", 5),
		RenMockInterval: beego.AppConfig.DefaultInt("RenMockInterval", 15),
		HuoMockInterval: beego.AppConfig.DefaultInt("HuoMockInterval", 5),
	}
	return conf
}

func checkMockStartPositions() bool {
	return mockutils.StartMockRuntime(getMockConfig())
}

func checkMockStartAlarm() bool {
	return mockutils.StartMockRuntimeHuo(getMockConfig())
}

package controllers

import (
	"encoding/json"
	"errors"
	"gitee.com/nilyang/monitor/server/models"
	"strconv"
	"strings"

	"gitee.com/nilyang/monitor/server/ws"
	"gitee.com/nilyang/monitor/utils"
	"github.com/astaxie/beego"
)

var hub *ws.Hub

func init() {
	hub = ws.NewHub()
	go hub.Run()
}

//  MessageController operations for Message
type MessageController struct {
	beego.Controller
}

// URLMapping ...
func (c *MessageController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// websocket 连接
func (c *MessageController) WsMsg() {
	c.EnableRender = false
	ws.ServeWebsocket(hub, c.Ctx.ResponseWriter, c.Ctx.Request)
}

// 接收消息
func (c *MessageController) WsSendMsg() {
	c.EnableRender = false
	msg := c.GetString("msg")
	location := c.GetString("location")
	pic := c.GetString("pic")

	msgjson := map[string]interface{}{}
	msgjson["msg"] = msg
	msgjson["location"] = location
	msgjson["pic"] = pic
	msgBytes, err := json.Marshal(msgjson)
	if err != nil {
		c.Data["json"] = utils.JsonError("fail")
	} else {
		ws.SendMessage(hub, msgBytes)
		c.Data["json"] = utils.JsonSuccess("ok")
	}

	c.ServeJSON()
}

// Post ...
// @Title Post
// @Description create Message
// @Param	body		body 	models.Message	true		"body for Message content"
// @Success 201 {int} models.Message
// @Failure 403 body is empty
// @router / [post]
func (c *MessageController) Post() {
	var v models.Message
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)

	body, err := utils.SendJson("/api/v1/message", v)
	if err != nil {
		c.Data["json"] = utils.JsonError(err)
	} else {
		c.Data["json"] = body
		v.Body = body

		if _, err := models.AddMessage(&v); err == nil {
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = utils.JsonSuccess(v)
		} else {
			c.Data["json"] = utils.JsonError(err.Error())
		}
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get Message by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Message
// @Failure 403 :id is empty
// @router /:id [get]
func (c *MessageController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v, err := models.GetMessageById(id)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = v
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get Message
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Message
// @Failure 403
// @router / [get]
func (c *MessageController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = utils.JsonError(errors.New("Error: invalid query key/value pair"))
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllMessage(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = utils.JsonError(err.Error())
	} else {
		c.Data["json"] = utils.JsonSuccess(l)
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the Message
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Message	true		"body for Message content"
// @Success 200 {object} models.Message
// @Failure 403 :id is not int
// @router /:id [put]
func (c *MessageController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v := models.Message{Id: id}
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if err := models.UpdateMessageById(&v); err == nil {
		c.Data["json"] = utils.JsonSuccess("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the Message
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *MessageController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if err := models.DeleteMessage(id); err == nil {
		c.Data["json"] = utils.JsonSuccess("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}
	c.ServeJSON()
}

package controllers

import "github.com/astaxie/beego"

type ErrorController struct {
	beego.Controller
}

func (c *ErrorController) Error404() {
	if c.Ctx.Input.IsAjax() {
		c.Data["json"] = map[string]interface{}{
			"code": 404,
			"data": "Page not found",
		}
		c.ServeJSON()
	} else {
		c.Data["content"] = "Page not found"
		c.TplName = "404.tpl"
	}
}

func (c *ErrorController) Error405() {
	if c.Ctx.Input.IsAjax() {
		c.Data["json"] = map[string]interface{}{
			"code": 405,
			"data": "Page not allowed",
		}
		c.ServeJSON()
	} else {
		c.Data["content"] = "Page not found"
		c.TplName = "405.tpl"
	}
}

func (c *ErrorController) Error500() {
	if c.Ctx.Input.IsAjax() {
		c.Data["json"] = map[string]interface{}{
			"code": 500,
			"data": "Server error",
		}
		c.ServeJSON()
	} else {
		c.Data["content"] = "Server error"
		c.TplName = "500.tpl"
	}
}

func (c *ErrorController) ErrorDb() {
	if c.Ctx.Input.IsAjax() {
		c.Data["json"] = map[string]interface{}{
			"code": 500,
			"data": "Server error",
		}
		c.ServeJSON()
	} else {
		c.Data["content"] = "database is now down"
		c.TplName = "dberror.tpl"
	}
}

package controllers

import (
	"encoding/json"

	"gitee.com/nilyang/monitor/server/models"

	"crypto/md5"
	"fmt"

	"github.com/astaxie/beego"
	"github.com/tidwall/gjson"

	//"log"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"
)

// Operations about Users
type UserController struct {
	beego.Controller
}

// URLMapping ...
func (c *UserController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.Get)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// @Title AddUser
// @Description create users
// @Param	body		body 	models.UserData	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router / [post]
func (u *UserController) Post() {
	uu := models.UserForm{}
	body := u.Ctx.Input.RequestBody
	if gjson.Valid(string(body)) == false {
		u.Data["json"] = utils.JsonError("Json format error")
		u.ServeJSON()
		return
	}
	json.Unmarshal(body, &uu)

	//{{{ 事务开始
	data, err := models.AddUserForm(&uu)
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}
	//}}}
	u.Data["json"] = utils.JsonSuccess(data)
	u.ServeJSON()
}

// @Title CreateUser
// @Description create users
// @Param	body		body 	models.UserData	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /init [post]
func (u *UserController) PostInit() {
	uu := models.UserData{}
	body := u.Ctx.Input.RequestBody
	if gjson.Valid(string(body)) == false {
		u.Data["json"] = utils.JsonError("Json Format Error")
		u.ServeJSON()
		return
	}
	json.Unmarshal(body, &uu)

	uu.Password = fmt.Sprintf("%x", md5.Sum([]byte(uu.Password)))

	gid, err := models.AddGroup(&uu.Group)
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}
	uu.Group.Id = gid
	pid, err := models.AddProfile(&uu.Profile)
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}
	uu.Profile.Id = pid
	did, err := models.AddDevice(&uu.Device)
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}
	uu.Device.Id = did

	ux := models.User{}
	ux.Username = uu.Username
	ux.Password = uu.Password
	ux.IsLead = uu.IsLead
	ux.Group = &uu.Group
	ux.Profile = &uu.Profile
	ux.Device = &uu.Device
	uid, err := models.AddUser(&ux)
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}

	u.Data["json"] = utils.JsonSuccess(map[string]int64{"uid": uid, "gid": gid, "pid": pid, "did": did})
	u.ServeJSON()
}

// @Title GetAll
// @Description get all Users
// @Success 200 {object} models.User
// @router / [get]
func (u *UserController) GetAll() {
	var (
		page   int
		fields []string
	)
	u.Ctx.Input.Bind(&page, "page")

	fields = []string{"Id", "Username", "Realname", "IsLead", "Role", "Profile", "Device", "Group"}
	users, err := models.GetAllUsers(page, fields)

	if err != nil {
		u.Data["json"] = utils.JsonError(err)
	} else {
		u.Data["json"] = utils.JsonSuccess(users)
	}
	u.ServeJSON()
}

// @Title Get
// @Description get user by uid
// @Param	uid	path	int	true	"The key for staticblock"
// @Success 200 {object} models.User
// @Failure 403 :uid is empty
// @router /:uid [get]
func (u *UserController) Get() {
	uid, err := u.GetInt64(":uid")
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
		u.ServeJSON()
		return
	}
	if uid != 0 {
		user, err := models.GetUser(uid)
		if err != nil {
			u.Data["json"] = utils.JsonError(err.Error())
		} else {
			u.Data["json"] = utils.JsonSuccess(user)
		}
	}
	u.ServeJSON()
}

// @Title Update
// @Description update the user
// @Param	uid		path 	string	true		"The uid you want to update"
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {object} models.User
// @Failure 403 :uid is not int
// @router /:uid [put]
func (u *UserController) Put() {
	uid, err := u.GetInt64(":uid")
	if err != nil {
		panic(err)
	}
	if uid != 0 {
		var user models.User
		json.Unmarshal(u.Ctx.Input.RequestBody, &user)
		if user.Password != "" {
			user.Password = fmt.Sprintf("%x", md5.Sum([]byte(user.Password)))
		}
		uu, err := models.UpdateUser(uid, &user)
		if err != nil {
			u.Data["json"] = utils.JsonError(err.Error())
		} else {
			u.Data["json"] = utils.JsonSuccess(uu)
		}
	}
	u.ServeJSON()
}

// @Title Delete
// @Description delete the user
// @Param	uid	path	int	true	"The uid you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 uid is empty
// @router /:uid [delete]
func (u *UserController) Delete() {
	uid, err := u.GetInt64(":uid")
	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
	} else {
		models.DeleteUser(uid)
		u.Data["json"] = utils.JsonSuccess("delete success!")
	}
	u.ServeJSON()
}

// @Title Login
// @Description Logs user into the system
// @Param	username		query 	string	true		"The username for login"
// @Param	password		query 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /login [post]
func (u *UserController) Login() {
	body := u.Ctx.Input.CopyBody(int64(1024))
	//log.Printf("body:%s", string(body))

	user := models.User{}
	err := json.Unmarshal(body, &user)

	if err != nil {
		u.Data["json"] = utils.JsonError(err.Error())
	}
	username := user.Username
	password := user.Password

	//log.Printf("username: %s, password: %s", username, password)
	//for k,v := range u.Input() {
	//log.Printf("key:%s, val:%s", k,v)
	//}

	if user, ok := models.Login(username, password); ok {
		payload := structs.Payload{"username": user.Username, "uid": user.Id}
		token, err := utils.JwtMakeToken(utils.SECRET_KEY, payload)
		if err != nil {
			u.Data["json"] = utils.JsonError("token 获取失败")
		} else {
			u.Data["json"] = utils.JsonSuccess(map[string]interface{}{"token": token, "uid": user.Id})
		}
	} else {
		u.Data["json"] = utils.JsonError("用户名或密码错误，登录失败")
	}
	u.ServeJSON()
}

// @Title logout
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [get]
func (u *UserController) Logout() {
	u.Data["json"] = utils.JsonSuccess("OK")
	u.ServeJSON()
}

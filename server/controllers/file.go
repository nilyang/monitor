package controllers

import (
	"encoding/json"
	"errors"
	"gitee.com/nilyang/monitor/server/models"
	"gitee.com/nilyang/monitor/structs"

	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"log"
	"gitee.com/nilyang/monitor/utils"
	"os"
)

//  FileController operations for File
type FileController struct {
	beego.Controller
}

// URLMapping ...
func (c *FileController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description create File
// @Param	body		body 	models.File	true		"body for File content"
// @Success 201 {int} models.File
// @Failure 403 body is empty
// @router / [post]
func (c *FileController) Post() {
	// upload
	file, fheader, err := c.GetFile("image")
	if err != nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = utils.JsonError(err.Error())
	}
	defer file.Close()

	var dir_sep = string(os.PathSeparator)
	var file_path string = os.TempDir() + dir_sep + "Monitor" + dir_sep + "Upload"

	if _, err := os.Stat(file_path); os.IsNotExist(err) {
		err := os.MkdirAll(file_path, os.ModePerm)
		if err != nil {
			c.Ctx.Output.Status = 500
			c.Data["json"] = utils.JsonError(err.Error())
			c.ServeJSON()
			return
		}
	}
	file_path += dir_sep + fheader.Filename
	//log.Println("[tmp path] --> " + file_path)
	err = c.SaveToFile("image", file_path)
	if err != nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = utils.JsonError(err.Error())
		c.ServeJSON()
		return
	}

	// 计算文件md5 hash 用以去重
	hash, err := utils.Md5sumFile(file_path)
	if err != nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = utils.JsonError(err.Error())
		c.ServeJSON()
		return
	}

	data, err := models.GetFileByHash(hash)
	if err != nil && err != orm.ErrNoRows {
		// 数据库错误
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = utils.JsonError(err.Error())
		c.ServeJSON()
		return
	}

	if err == nil {
		// 已存在，则不再存到云存储，节约空间
		data.PublicUrl = "http://" + data.Url + "/" + structs.FormatCloudPathByFid(data.Fid)
		if _, err := models.UpdateFileById(data); err == nil {
			result := make(map[string]interface{})
			result["id"] = data.Id
			result["url"] = data.PublicUrl
			result["hash"] = data.Hash
			c.Data["json"] = utils.JsonSuccess(result)
		} else {
			c.Data["json"] = utils.JsonError(err.Error())
		}
	} else if err == orm.ErrNoRows {
		// 无记录，则存到云存储中
		// OK save to cloud
		info, err := utils.CloudSave(file_path)
		if err != nil {
			c.Data["json"] = utils.JsonError(err.Error())
			c.ServeJSON()
			return
		}
		var v models.File
		v.Hash = hash
		v.Filename = fheader.Filename
		v.Mime = fheader.Header.Get("Content-Type")
		v.Fid = info.Fid
		v.Url = info.Url
		v.PublicUrl = "http://" + info.PublicUrl + "/" + structs.FormatCloudPathByFid(info.Fid)
		v.Size = info.Size

		if id, err := models.AddFile(&v); err == nil {
			v.Id = id
			c.Ctx.Output.SetStatus(201)

			result := make(map[string]interface{})
			result["id"] = v.Id
			result["url"] = v.PublicUrl
			result["hash"] = v.Hash
			result["fid"] = v.Fid

			c.Data["json"] = utils.JsonSuccess(result)
		} else {
			c.Data["json"] = utils.JsonError(err.Error())
		}
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get File by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.File
// @Failure 403 :id is empty
// @router /:id [get]
func (c *FileController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v, err := models.GetFileById(id)
	if err != nil {
		c.Data["json"] = utils.JsonError(err.Error())
	} else {
		c.Data["json"] = utils.JsonSuccess(v)
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get File
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.File
// @Failure 403
// @router / [get]
func (c *FileController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = utils.JsonError(errors.New("Error: invalid query key/value pair").Error())
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllFile(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = utils.JsonError(err.Error())
	} else {
		c.Data["json"] = utils.JsonSuccess(l)
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the File
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.File	true		"body for File content"
// @Success 200 {object} models.File
// @Failure 403 :id is not int
// @router /:id [put]
func (c *FileController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v := models.File{Id: id}
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if _, err := models.UpdateFileById(&v); err == nil {
		c.Data["json"] = utils.JsonSuccess("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the File
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *FileController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if err := models.DeleteFile(id); err == nil {
		c.Data["json"] = utils.JsonSuccess("OK")
	} else {
		c.Data["json"] = utils.JsonError(err.Error())
	}
	c.ServeJSON()
}

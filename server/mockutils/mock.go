package mockutils

import (
	"fmt"
	"time"

	"gitee.com/nilyang/monitor/utils"
)

const (
	TaskTypeRen          = "ren"
	TaskTypeChe          = "che"
	TaskTypeHuo          = "huo"
	TypeRen              = 1
	TypeChe              = 2
	TaskStatusNotRun     = 0
	TaskStatusRunning    = 1
	TaskStatusFinished   = 2
	RuntimeStatusNotRun  = 0
	RuntimeStatusRuning  = 1
	RuntimeStatusAllDone = 2
)

type MockConfig struct {
	CheMockInterval int
	RenMockInterval int
	HuoMockInterval int
}

// 单例
var MockRuntime, MockRuntimeHuo *MockDataRuntime

// 人、车 分组，按多个坐标集合（即设备坐标集合）
type MockPoints struct {
	Huo    utils.Point     `json:"huo"`
	Che    [][]utils.Point `json:"che"`
	Ren    [][]utils.Point `json:"ren"`
	HuoPic []string        `json:"huo_pic"`
}

func (mp MockPoints) GetCheCounts() int {
	return len(mp.Che)
}

func (mp MockPoints) GetRenCounts() int {
	return len(mp.Ren)
}

func (mp MockPoints) GetCheNthPointCounts(nth int) int {
	return len(mp.Che[nth])
}

func (mp MockPoints) GetRenNthPointCounts(nth int) int {
	return len(mp.Ren[nth])
}

func (mp MockPoints) GetHuoPoint() utils.Point {
	return mp.Huo
}

// GPS位置上报模拟器
type MockTask struct {
	mockRuntime *MockDataRuntime // 运行时引用
	Tick        *time.Ticker     // 计数器

	Type       string        // 任务类型 ren,che,huo
	Status     int           // 任务执行状态 0 未执行 1 执行中 2 已完成
	Elapse     time.Duration // 任务已经执行的秒数
	Interval   time.Duration // 获取下一个位置的时间间隔
	UpdateTime time.Time     // 更新时间

	// ren/che
	DeviceIndex       int         // GPS 设备索引
	DevicePointCounts int         // GPS 上报位点位置数量
	CurrentPointIndex int         // 当前位置索引（0~N）
	CurrentPoint      utils.Point // 当前位置

	// huo
	PicCounts       int    // 火图个数
	CurrentPicName  string // 当前火图文件名
	CurrentPicIndex int    // 当前火图索引

}

// 更新当前位置（点）坐标
func (task *MockTask) updateDevicePoint() {
	// 位置数量
	data := task.mockRuntime.MockData
	if task.CurrentPointIndex == (task.DevicePointCounts - 1) {
		// 任务已经执行完毕，不需再执行
		task.finished()
		return
	}

	if task.Type == TaskTypeRen {
		task.CurrentPoint = data.Ren[task.DeviceIndex][task.CurrentPointIndex]
	} else if task.Type == TaskTypeChe {
		task.CurrentPoint = data.Che[task.DeviceIndex][task.CurrentPointIndex]
	}

	task.CurrentPointIndex += 1 // 不能大于设备点数

	//if task.Type == TaskTypeChe {
	//	fmt.Printf("[%s%d] task point = %s\n", task.Type, task.DeviceIndex, task.CurrentPoint.ToString())
	//}
}

// huo pic update
func (task *MockTask) updateHuoPic() {
	data := task.mockRuntime.MockData

	if task.CurrentPicIndex <= (task.PicCounts - 1) {
		task.CurrentPicName = data.HuoPic[task.CurrentPicIndex]
	}

	fmt.Printf("[%s%d] task pic index = %d, picName = %s\n", task.Type, task.DeviceIndex, task.CurrentPicIndex, task.CurrentPicName)

	if task.CurrentPicIndex == (task.PicCounts - 1) {
		// 任务已经执行完毕，不需再执行
		task.finished()
		return
	}

	task.CurrentPicIndex += 1
}

// 启动gps上报模拟协程，非阻塞
// 每到一个设定的时间间隔，就按顺序取出一个坐标，
// 将当前位置移动到该位置（当前位置数据替换为新位置数据）
func (task *MockTask) Start() {
	task.Status = TaskStatusRunning
	if task.Type == TaskTypeHuo {
		task.CurrentPicIndex = 0
	} else {
		task.CurrentPointIndex = 0
	}
	go func() {
		task.Tick = time.NewTicker(time.Second * 1)
		for {
			select {
			case <-task.Tick.C:
				if task.Elapse%task.Interval == 0 {
					task.UpdateTime = time.Now()
					if task.Type == TaskTypeHuo {
						task.updateHuoPic()
					} else {
						task.updateDevicePoint()
					}
				}
				task.Elapse += 1
			}
		}
	}()
}

func (task *MockTask) finished() {
	task.Status = TaskStatusFinished
	task.Tick.Stop()
	fmt.Printf("[%s%d]task finished\n", task.Type, task.DeviceIndex)
}

func (task *MockTask) GetCurrentPoint() utils.Point {
	return task.CurrentPoint
}
func (task *MockTask) GetCurrentPicName() string {
	return task.CurrentPicName
}

func (task *MockTask) GetUpdateTime() time.Time {
	return task.UpdateTime
}

// 全部位置模拟数据运行时
type MockDataRuntime struct {
	taskRuns  map[string]*MockTask // 任务执行运行时
	MockData  MockPoints           // 人车火位置跟踪，模拟数据
	Status    int
	startTime time.Time
}

func NewMockDataRuntime() *MockDataRuntime {
	mtt := new(MockDataRuntime)
	mtt.taskRuns = make(map[string]*MockTask) // 初始化
	mtt.MockData = GetMockDataFromFiles()
	mtt.Status = RuntimeStatusNotRun
	mtt.startTime = time.Now()
	return mtt
}

func (m MockDataRuntime) GetStartTime() time.Time {
	return m.startTime
}

func (m *MockDataRuntime) IsRuning() bool {
	return m.Status == RuntimeStatusRuning
}

func (m *MockDataRuntime) UpdateMockData() {
	m.MockData = GetMockDataFromFiles()
}

// 启动一个设备的坐标移动模拟
func (m *MockDataRuntime) NewTickerTask(taskType string, taskIdx int, interval time.Duration) *MockTask {
	var task *MockTask
	taskName := fmt.Sprintf("%s%d", taskType, taskIdx)
	task, fund := m.taskRuns[taskName]
	if fund {
		return task
	}

	// 创建任务
	task = new(MockTask)
	task.Status = TaskStatusNotRun
	task.Interval = interval
	task.Type = taskType
	task.DeviceIndex = taskIdx

	if taskType == TaskTypeHuo {
		// 火pic数
		task.PicCounts = len(m.MockData.HuoPic)
		fmt.Printf("task.PicCounts = %d\n", task.PicCounts)
	} else {
		// 车、人 位置点数
		var pointCounts int
		if taskType == TaskTypeChe {
			pointCounts = len(m.MockData.Che[taskIdx])
		} else if taskType == TaskTypeRen {
			pointCounts = len(m.MockData.Ren[taskIdx])
		}

		task.DevicePointCounts = pointCounts
	}

	task.mockRuntime = m // 反向引用

	// 开始任务
	task.Start()

	fmt.Printf("task [%s] runing...\n", taskName)
	// 任务绑定到运行时
	m.taskRuns[taskName] = task
	m.Status = RuntimeStatusRuning
	return task
}

func (m *MockDataRuntime) GetCurrentTask(taskIdx int, taskType string) *MockTask {
	var task *MockTask
	for _, v := range m.taskRuns {
		if v.Type == taskType && v.DeviceIndex == taskIdx {
			task = v
			break
		}
	}

	return task
}

func GetRuntimeMockData() MockPoints {
	return MockRuntime.MockData
}

// 当前任务
func GetCurrentTask(taskIdx int, taskType string) *MockTask {
	return MockRuntime.GetCurrentTask(taskIdx, taskType)
}

// 人/车 任务当前位置
func GetCurrentTaskPoint(taskIdx int, taskType string) utils.Point {
	return MockRuntime.GetCurrentTask(taskIdx, taskType).GetCurrentPoint()
}

func GetCurrentHuoPoint() utils.Point {
	return MockRuntimeHuo.MockData.Huo
}

func GetCurrentHuoPic() string {
	return MockRuntimeHuo.GetCurrentTask(0, TaskTypeHuo).GetCurrentPicName()
}

func GetFireAlarmTime() time.Time {
	return MockRuntimeHuo.GetStartTime()
}

func startMockDataService(waitAllDone chan bool, conf MockConfig) {
	mockRuntime := NewMockDataRuntime()

	renCount := mockRuntime.MockData.GetRenCounts()
	cheCount := mockRuntime.MockData.GetCheCounts()

	var cheTaskInterval = time.Duration(conf.CheMockInterval) // 车走得快
	var renTaskInterval = time.Duration(conf.RenMockInterval) // 人走得慢

	for idx := 0; idx < cheCount; idx++ {
		mockRuntime.NewTickerTask(TaskTypeChe, idx, cheTaskInterval)
	}

	for idx := 0; idx < renCount; idx++ {
		mockRuntime.NewTickerTask(TaskTypeRen, idx, renTaskInterval)
	}

	// 检查所有任务是否均已完成
	// 重新来过？
	go func() {
		ticker := time.NewTicker(time.Second * 1)
		for {
			select {
			case <-ticker.C:
				isAllFinished := true
				for _, task := range mockRuntime.taskRuns {
					isAllFinished = isAllFinished && task.Status == TaskStatusFinished
				}
				if isAllFinished {
					ticker.Stop()
					fmt.Println("All [gps] task finished")
					waitAllDone <- true
					mockRuntime.Status = RuntimeStatusAllDone
				}
			}
		}
	}()

	MockRuntime = mockRuntime
}

// 开始坐标模拟数据
func StartMockRuntime(conf MockConfig) bool {
	if MockRuntime != nil && MockRuntime.Status == RuntimeStatusRuning {
		return false
	}

	waitAllDone := make(chan bool)
	startMockDataService(waitAllDone, conf)
	go func() {
		<-waitAllDone
		fmt.Println("Mock [gps] runtime All Done!")
	}()

	return true
}

func startMockDataServiceHuo(waitAllDone chan bool, conf MockConfig) {
	mockRuntime := NewMockDataRuntime()
	var huoPicInterval = time.Duration(conf.HuoMockInterval) // 火更新频率 5秒固定
	mockRuntime.NewTickerTask(TaskTypeHuo, 0, huoPicInterval)

	go func() {
		ticker := time.NewTicker(time.Second * 1)
		for {
			select {
			case <-ticker.C:
				isAllFinished := true
				for _, task := range mockRuntime.taskRuns {
					isAllFinished = isAllFinished && task.Status == TaskStatusFinished
				}
				if isAllFinished {
					ticker.Stop()
					fmt.Println("All huo task finished")
					waitAllDone <- true
					mockRuntime.Status = RuntimeStatusAllDone
				}
			}
		}
	}()

	MockRuntimeHuo = mockRuntime
}

func StartMockRuntimeHuo(conf MockConfig) bool {
	if MockRuntimeHuo != nil && MockRuntimeHuo.Status == RuntimeStatusRuning {
		return false
	}

	waitAllDone := make(chan bool)
	startMockDataServiceHuo(waitAllDone, conf)
	go func() {
		<-waitAllDone
		fmt.Println("Mock [huo] runtime All Done!")
	}()

	return true
}

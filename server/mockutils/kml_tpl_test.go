package mockutils

import (
	"bytes"
	"html/template"
	"log"
	"testing"
)

func TestRenderKML(t *testing.T) {
	const tpl = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
	</head>
	<body>
		{{range .Items}}<div>{{ . }}</div>{{else}}<div><strong>no rows</strong></div>{{end}}
	</body>
</html>`

	data := make(KmlTemplate)
	data["Title"] = "Too young to simple!"
	data["Items"] = []string{"Foo", "OO", "XX"}

	str := RenderKML(data, tpl)

	log.Printf("\n%s\n", string(str))

}

func TestRenderHTML(tt *testing.T) {
	const tpl = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
	</head>
	<body>
		{{range .Items}}<div>{{ . }}</div>{{else}}<div><strong>no rows</strong></div>{{end}}
	</body>
</html>`

	check := func(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}
	t, err := template.New("webpage").Parse(tpl)
	check(err)

	data := struct {
		Title string
		Items []string
	}{
		Title: "My page",
		Items: []string{
			"My photos",
			"My blog",
		},
	}

	buf := bytes.NewBufferString("")
	err = t.Execute(buf, data)
	check(err)

	noItems := struct {
		Title string
		Items []string
	}{
		Title: "My another page",
		Items: []string{},
	}

	err = t.Execute(buf, noItems)
	check(err)
	// bufferd and there is no out put here

}

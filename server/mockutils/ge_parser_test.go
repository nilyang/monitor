package mockutils

import (
	"fmt"
	"path/filepath"
	"regexp"
	"strconv"
	"testing"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/utils"
)

func TestPathSeperator(t *testing.T) {
	path := filepath.Join(libs.PATH_SEPERRATOR, "static", "earth", "huo")
	sep := "/"
	if libs.PATH_SEPERRATOR == "\\" {
		sep = "\\\\"
	}
	reg := regexp.MustCompile(sep)
	vv := reg.Split(path, -1)
	fmt.Printf("vv_top = %s\n", vv[len(vv)-1])

}

func TestMakeLine(t *testing.T) {
	pathStr := `106.7126457621527,29.66656330312978,0 106.7125478617201,29.66682847124765,0 106.712380793011,29.66716217999849,0 106.7121916175632,29.66734182568341,0 106.7120617835269,29.6677223814148,0 106.7120007786096,29.66812458146762,0 106.7119751133105,29.66840532674904,0 106.7120069871087,29.66863955354311,0 106.7119218727322,29.66905146134904,0 106.711859928762,29.66928743107319,0 106.711803484034,29.66959385755069,0 106.7118154059095,29.6698478315365,0 106.7118558269685,29.66999184305518,0 106.7119066954448,29.67008591073241,0 106.7119855999022,29.67019622833666,0 106.7120415184729,29.67028805250715,0 106.7121268957055,29.67042154112491,0 106.7120366281733,29.67054574957028,0 106.7120194938095,29.67066033462645,0 106.7121935338239,29.6708475172697,0 106.7120084821652,29.67089274095031,0`
	sep := "\\s"
	reg := regexp.MustCompile(sep)
	vv := reg.Split(pathStr, -1)
	fmt.Printf("%d\n", len(vv))
	for _, pointStr := range vv {
		reg = regexp.MustCompile(",")
		pArr := reg.Split(pointStr, -1)
		p := utils.Point{}

		p.Lng, _ = strconv.ParseFloat(pArr[0], 64)
		p.Lat, _ = strconv.ParseFloat(pArr[1], 64)
		p.Alt, _ = strconv.ParseFloat(pArr[2], 64)

		d1, f1, m1 := utils.MakeDegMinSec(p.Lat)
		d, f, m := utils.MakeDegMinSec(p.Lng)
		fmt.Printf(" %.0f°%.0f′%.2f″北\n%.0f°%.0f′%.2f″东\n\n", d1, f1, m1, d, f, m)
	}
}

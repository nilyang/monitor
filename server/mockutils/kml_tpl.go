package mockutils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"text/template"

	"gitee.com/nilyang/monitor/server/models"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"
)

// 出发点
var StartPoint = utils.Point{Lat: 106.7124416666667, Lng: 29.67186388888889}

type KmlTemplate map[string]interface{}

func GetKmlContent(kmlName string) string {
	xmlFile := GetMockDataDir() + fmt.Sprintf("/kml/%s.kml", kmlName)

	var kmlByte []byte

	if tmp, err := ioutil.ReadFile(xmlFile); err != nil {
		panic(err)
	} else {
		kmlByte = tmp
	}
	return string(kmlByte)
}

func RenderKML(data KmlTemplate, tpl string) []byte {
	var funcMap template.FuncMap
	funcMap = make(template.FuncMap)

	funcMap["getName"] = func(pView structs.PositionView) string {
		return pView.Name
	}

	funcMap["toKmlPointString"] = func(pView structs.PositionView) string {
		return pView.ToKmlPointString()
	}

	buf := bytes.NewBufferString("")
	t := template.New("hello").Funcs(funcMap)

	t.Parse(tpl)
	t.Execute(buf, data)

	return buf.Bytes()
}

func RenderHuo(addr string) []byte {
	var data = make(KmlTemplate)

	alarms, err := models.GetAlarmsForKml()
	if err != nil {
		return nil
	}

	if len(alarms) > 0 {
		fire := alarms[0]
		p := utils.Point{Lng: fire.FireLongitude, Lat: fire.FireLatitude}
		p1, p2 := utils.MakeSquare(p.Lat, p.Lng, 150, 100)

		data["North"] = p1.Lat
		data["South"] = p2.Lat
		data["East"] = p2.Lng
		data["West"] = p1.Lng
		data["HuoPic"] = fire.Images[0]
	} else {
		data["North"] = "0"
		data["South"] = "0"
		data["East"] = "0"
		data["West"] = "0"
		data["HuoPic"] = fmt.Sprintf("%s/static/earth/6d6.png", addr)
	}

	tpl := GetKmlContent("huo1")

	return RenderKML(data, tpl)
}

func RenderRen(addr string) []byte {

	var data = make(KmlTemplate)
	data["Addr"] = addr
	data["Placemarks"], _ = models.GetPositionsForKml(TypeRen)

	tpl := GetKmlContent("jiuyuan1")

	return RenderKML(data, tpl)
}

func RenderChe(addr string) []byte {
	var data = make(KmlTemplate)
	data["Addr"] = addr

	data["Placemarks"], _ = models.GetPositionsForKml(TypeChe)

	tpl := GetKmlContent("jiuyuanche1")

	return RenderKML(data, tpl)
}

func GetMockPositionViews(pType int) []structs.PositionView {
	var pViews []structs.PositionView

	//tmp := MockPositionData()

	pViews = MockPositionDataV2(pType)

	return pViews
}

// 人车位置模拟v1
func MockPositionData() []structs.PositionView {
	json_str := `
[
    {
        "id":"97af092c06534fd9a22be0755b1346e8",
        "name":"李某某",
        "type":1,
        "gpsNumber":"G0-132",
        "longitude":105.85964722222222,
        "latitude":29.30400277777778,
        "writeTime":"2017-11-09T10:11:35"
    },
    {
        "id":"6a9a9e62ecb64a798718e099e34e0fdb",
        "name":"消防车",
        "type":2,
        "gpsNumber":"G0-170",
        "longitude":106.82113333333334,
        "latitude":29.495975,
        "writeTime":"2017-11-09T10:16:29"
    },
    {
        "id":"eccbc87e4b5ce2fe28308fd9f2a7baf3",
        "name":"救援车",
        "type":2,
        "gpsNumber":"G0-200",
        "longitude":106.82113333333334,
        "latitude":29.495975,
        "writeTime":"2017-11-09T10:16:29"
    }
]
`
	var positions []structs.Position
	var positionView structs.PositionView
	var runtumePoints []structs.PositionView

	json.Unmarshal([]byte(json_str), &positions)
	if MockRuntime == nil {
		return []structs.PositionView{}
	}

	cheWzIdx := 0
	renWzIdx := 0
	//TODO positions 替换为扫描到的模拟任务数量分别遍历
	for _, v := range positions {
		var currPoint utils.Point
		if v.Type == 1 {
			currPoint = GetCurrentTaskPoint(renWzIdx, TaskTypeRen)
			renWzIdx++
		} else if v.Type == 2 {
			currPoint = GetCurrentTaskPoint(cheWzIdx, TaskTypeChe)
			cheWzIdx++
		}

		//v.WriteTime = //TODO 发生时间暂不模拟

		positionView.Id = v.Id
		positionView.Type = v.Type
		positionView.Name = v.Name
		positionView.GpsNumber = v.GpsNumber
		positionView.WriteTime = v.WriteTime
		positionView.Longitude = fmt.Sprintf("%.16f", currPoint.Lng)
		positionView.Latitude = fmt.Sprintf("%.16f", currPoint.Lat)
		positionView.Altitude = fmt.Sprintf("%.0f", currPoint.Alt)
		runtumePoints = append(runtumePoints, positionView)
	}

	return runtumePoints
}

// 火位置模拟
func GetMockFireData(urlHost string) []structs.Alarm {
	var json_str string = `[
    {
        "id":1,
        "name":"S2",
        "stationId":"50010340005026402555",
        "stationLongitude":105.85964722222222,
        "stationLatitude":29.30400277777778,
        "fireLongitude":106.60823333333332,
        "fireLatitude":29.92278611111111,
        "alarmTime":"2017-10-12T15:43:26",
        "images":[
            "http://localhost:8080/static/img/fire100.jpg",
            "http://localhost:8080/static/img/fire102.jpg"
        ]
    },
    {
        "id":2,
        "name":"S3",
        "stationId":"50010340005026402556",
        "stationLongitude":106.82113333333334,
        "stationLatitude":29.495975,
        "fireLongitude":106.59156666666668,
        "fireLatitude":29.75645555555555,
        "alarmTime":"2017-10-12T15:43:33",
        "images":[
            "http://localhost:8080/static/img/fire106.jpg",
            "http://localhost:8080/static/img/fire107.jpg"
        ]
    }
]`
	var alarms []structs.Alarm
	json.Unmarshal([]byte(json_str), &alarms)

	alrmTime := GetFireAlarmTime()
	huo := GetCurrentHuoPoint()
	fireOne := alarms[0]
	fireOne.FireLatitude = huo.Lat
	fireOne.FireLongitude = huo.Lng
	fireOne.AlarmTime, _ = libs.TimeFormatToT(alrmTime.Format(libs.FMT_YYYYMMDD_hhmmss))

	picName := GetCurrentHuoPic()
	picUrl := ""
	if libs.IsEmpty(picName) {
		picUrl = fmt.Sprintf("%s/static/earth/6d6.png", urlHost)
	} else {
		picUrl = fmt.Sprintf("%s/static/earth/huo/%s", urlHost, picName)
	}

	fireOne.Images[0] = picUrl
	var ones []structs.Alarm
	ones = append(ones, fireOne)
	return ones
}

// 人车位置模拟v2
// 支持多、多车，取决于相应的设备模拟文件数
func MockPositionDataV2(pType int) []structs.PositionView {
	var runtumePoints []structs.PositionView

	if MockRuntime == nil {
		return []structs.PositionView{}
	}

	mockData := GetRuntimeMockData()

	if pType == TypeRen || pType == 0 {
		counts := mockData.GetRenCounts()
		fmt.Printf("counts-ren: %d\n", counts)

		for i := 0; i < counts; i++ {
			tmp := GetCurrentTask(i, TaskTypeRen)
			pView := pointToPositionView(tmp, TypeRen)
			runtumePoints = append(runtumePoints, pView)
		}
	}
	if pType == TypeChe || pType == 0 {
		counts := mockData.GetCheCounts()
		fmt.Printf("counts-che: %d\n", counts)
		for i := 0; i < counts; i++ {
			tmp := GetCurrentTask(i, TaskTypeChe)
			pView := pointToPositionView(tmp, TypeChe)
			runtumePoints = append(runtumePoints, pView)
		}
	}

	return runtumePoints
}

func pointToPositionView(task *MockTask, pType int) structs.PositionView {
	var positionView structs.PositionView

	point := task.CurrentPoint
	var name, deviceNo string
	if task.Type == TaskTypeChe {
		name = fmt.Sprintf("救援车-%d", task.DeviceIndex+1)
		deviceNo = fmt.Sprintf("GPS-CHE-%d", task.DeviceIndex+1)
	} else {
		name = fmt.Sprintf("护林员-%d", task.DeviceIndex+1)
		deviceNo = fmt.Sprintf("GPS-REN-%d", task.DeviceIndex+1)
	}

	positionView.Id = utils.Md5sumString(deviceNo) // md5
	positionView.Type = pType
	positionView.Name = name
	positionView.GpsNumber = deviceNo
	positionView.WriteTime = task.GetUpdateTime().Format(libs.FMT_YYYYMMDD_hhmmss_T)
	positionView.Longitude = fmt.Sprintf("%.16f", point.Lng)
	positionView.Latitude = fmt.Sprintf("%.16f", point.Lat)
	positionView.Altitude = fmt.Sprintf("%.0f", point.Alt)

	return positionView
}

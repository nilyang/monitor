// GE 坐标解析器
package mockutils

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"gitee.com/nilyang/monitor/utils"

	"github.com/astaxie/beego"
)

//{{{ GE points string parser
func GetMockDataDir() string {
	return filepath.Join(beego.AppPath, "mockdata")
}

func GetMockStaticHuoDir() string {
	return filepath.Join(beego.AppPath, "static", "earth", "huo")
}

func GetMockDataFiles(namePattern string) []string {
	pattern := filepath.Join(GetMockDataDir(), namePattern)
	filesPaths, err := filepath.Glob(pattern)
	if err != nil {
		fmt.Printf("error: %v\n", err)
		return []string{}
	}
	//fmt.Printf("%s %v\n", namePattern, filesPaths)

	return filesPaths
}

func GetMockHuoPics(namePattern string) []string {
	pattern := filepath.Join(GetMockStaticHuoDir(), namePattern)
	filesPaths, err := filepath.Glob(pattern)
	if err != nil {
		fmt.Printf("error: %v\n", err)
		return []string{}
	}
	//fmt.Printf("%s %v\n", namePattern, filesPaths)

	return filesPaths
}

func GetMockDataFromFiles() MockPoints {
	cheFiles := GetMockDataFiles("che*.txt")
	renFiles := GetMockDataFiles("ren*.txt")
	huoFiles := GetMockDataFiles("huo.txt")
	huoPicsPng := GetMockHuoPics("*.png")
	huoPicsJpg := GetMockHuoPics("*.jpg")
	//for _, v := range huoFiles {
	//	fmt.Printf("huofile = %s\n", v)
	//}

	var (
		err       error
		huoWz     []byte
		cheWz     []byte
		renWz     []byte
		tmpPoints []utils.Point
		mockData  MockPoints
	)

	mockData.Che = make([][]utils.Point, len(cheFiles))
	mockData.Ren = make([][]utils.Point, len(renFiles))
	mockData.HuoPic = []string{}

	huoWz, err = ioutil.ReadFile(huoFiles[0])
	if err != nil {
		panic(err)
	}

	tmpPoints = utils.GEPointsParse(string(huoWz))

	mockData.Huo = tmpPoints[0]

	for i, v := range cheFiles {
		cheWz, _ = ioutil.ReadFile(v)
		mockData.Che[i] = utils.GEPointsParse(string(cheWz))
	}

	for i, v := range renFiles {
		renWz, _ = ioutil.ReadFile(v)
		mockData.Ren[i] = utils.GEPointsParse(string(renWz))
	}

	for _, v := range huoPicsPng {
		mockData.HuoPic = append(mockData.HuoPic, filepath.Base(v))
	}
	for _, v := range huoPicsJpg {
		mockData.HuoPic = append(mockData.HuoPic, filepath.Base(v))
	}

	fmt.Printf("mockData.HuoPic = %+v\n", mockData.HuoPic)

	return mockData
}

//}}}

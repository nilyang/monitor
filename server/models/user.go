package models

import (
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"

	"github.com/astaxie/beego/orm"
)

var (
	UserList map[int64]*User
	Users    []*UserInfo
)

func init() {
	orm.RegisterModel(new(User))
}

func AddUser(u *User) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(u)

	return id, err
}

func GetUser(uid int64) (u *UserInfo, err error) {
	o := orm.NewOrm()
	uu := User{Id: uid}
	err = o.Read(&uu)

	if err == orm.ErrNoRows {
		fmt.Println("查询不到")
		return nil, errors.New("User not exists")
	} else if err == orm.ErrMissPK {
		fmt.Println("找不到主键")
		return nil, errors.New("找不到主键")
	}

	fmt.Println(uu.Id, uu.Username)

	if uu.Profile != nil {
		o.Read(uu.Profile)
	}
	if uu.Device != nil {
		o.Read(uu.Device)
	}
	if uu.Group != nil {
		o.Read(uu.Group)
	}

	return ConvertUserToInfo(&uu), nil
}

func GetUserByName(username string) (*User, error) {
	o := orm.NewOrm()
	u := User{}
	u.Username = username
	err := o.Read(&u, "Username")
	if err == orm.ErrNoRows {
		log.Printf("未找到记录")
		return nil, errors.New("User not exists")
	} else if err == orm.ErrMissPK {
		log.Printf("找不到主键")
		return nil, errors.New("User table pk not exists")
	}

	return &u, nil
}

func GetAllUsers(page int, fields []string) (users []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(User))

	if page <= 1 {
		page = 1
	}
	limit := 10
	offset := (page - 1) * limit
	qs.Limit(10, offset)
	qs.OrderBy("-id")

	var list []User

	if _, err = qs.RelatedSel().All(&list, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range list {
				users = append(users, *ConvertUserToInfo(&v))
			}
			//
		} else {
			// trim unused fields
			for _, v := range list {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				json_byte, err := json.Marshal(m)
				if err != nil {
					return nil, err
				}
				var uu User
				m = nil
				json.Unmarshal(json_byte, &uu)
				//TODO 获取uu字段，转UserData
				users = append(users, *ConvertUserToInfo(&uu))
			}
		}
		return users, nil
	}

	return nil, err
}

func UpdateUser(uid int64, uu *User) (a *User, err error) {
	if u, ok := UserList[uid]; ok {
		if uu.Username != "" {
			u.Username = uu.Username
		}
		if uu.Password != "" {
			u.Password = fmt.Sprintf("%x", md5.Sum([]byte(uu.Password)))
		}
		//if uu.Profile.Age != 0 {
		//	u.Profile.Age = uu.Profile.Age
		//}
		//if uu.Profile.Address != "" {
		//	u.Profile.Address = uu.Profile.Address
		//}
		//if uu.Profile.Gender != "" {
		//	u.Profile.Gender = uu.Profile.Gender
		//}
		//if uu.Profile.Email != "" {
		//	u.Profile.Email = uu.Profile.Email
		//}
		//if uu.Profile.Role != "" {
		//	u.Profile.Role = uu.Profile.Role
		//}
		//if uu.Profile.Address != "" {
		//	u.Profile.Avatar = uu.Profile.Avatar
		//}
		return u, nil
	}
	return nil, errors.New("User Not Exist")
}

func Login(username, password string) (*User, bool) {
	u, err := GetUserByName(username)
	if err != nil {
		return nil, false
	}

	//log.Printf("db.pass:%s, form.pass:%x\n", u.Password, md5.Sum([]byte(password)))
	if u.Password == fmt.Sprintf("%x", md5.Sum([]byte(password))) {
		log.Printf("md5sum not equal")
		return u, true
	}

	return nil, false
}

func DeleteUser(id int64) (err error) {
	o := orm.NewOrm()
	v := Group{Id: id}

	// ascertain id exists in the database
	if err := o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Group{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}

func AddUserForm(uu *UserForm) (int64, error) {
	o := orm.NewOrm()

	newuser := User{}

	group, err := GetGroupById(uu.GroupId)
	if err != nil {
		return 0, err
	}

	device, err := GetDeviceById(uu.DeviceId)
	if err != nil {
		return 0, err
	}

	tmp_user := User{Username: uu.Username}
	err = o.Read(&tmp_user)
	if err == orm.ErrNoRows {
		//OK
	}

	if tmp_user.Id > 0 {
		return 0, errors.New("账号已存在，请重新输入账号")
	}

	newuser.Group = group
	newuser.Username = uu.Username
	newuser.IsLead = uu.IsLead
	newuser.Realname = uu.Realname
	newuser.Device = device

	// 最后更新profie
	profile := Profile{}
	profile.Age = uu.Age
	profile.Phone = uu.Phone
	profile.Gender = uu.Gender
	profile.UserId = newuser.Id

	o.Begin()

	pid, err := AddProfile(&profile)
	if err != nil {
		o.Rollback()
		return 0, err
	}

	profile.Id = pid
	newuser.Profile = &profile
	id, err := o.Insert(&newuser)
	if err != nil {
		o.Rollback()
		return 0, err
	}

	u := User{Id: id}
	err2 := o.Read(&u)
	if err2 != nil {
		o.Rollback()
		return 0, err2
	}

	profile.UserId = id
	err4 := UpdateProfileById(&profile)

	if err4 != nil {
		o.Rollback()
		return 0, err4
	}

	o.Commit()

	return id, err
}

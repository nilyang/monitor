package models

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"testing"

	"gitee.com/nilyang/monitor/utils"
)

func TestGetAlarmsForKml(t *testing.T) {
	mapFieldValues := []map[string]interface{}{{
		"Name": "Hello",
		"Point": `106.6082333333333168
29.9227861111111118`,
		"Location": `106.6082333333333168
29.9227861111111118`,
	}}

	for _, vMap := range mapFieldValues {
		//xMap := vMap.(map[string]interface{})
		xMap := vMap
		alarm := Alarm{}
		elem := reflect.ValueOf(&alarm).Elem() // 必须传入指针才可修改vv字段值
		for f, val := range xMap {
			elemVal := elem.FieldByName(f)
			if elemVal.IsValid() {
				elemVal.Set(reflect.ValueOf(val))
			}
		}

		reg := regexp.MustCompile(`(?:\r?\n(?:\r?\n)*|,\s*)`)
		arr := reg.Split(alarm.Point, -1)
		lat, _ := strconv.ParseFloat(arr[0], 32)
		lng, _ := strconv.ParseFloat(arr[1], 32)
		fmt.Printf("lat=%.16f, lng=%.16f\n", lat, lng)
		p1, p2 := utils.MakeSquare(lat, lng, 150, 100)
		fmt.Printf("%s\n%s\n", p1.ToString(), p2.ToString())
	}

}

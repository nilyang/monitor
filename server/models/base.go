package models

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

type DbConfig struct {
	Host   string
	Port   int
	User   string
	Pass   string
	DbName string
}

func GetDbConfig() DbConfig {
	config := DbConfig{
		"127.0.0.1",
		3306,
		"web",
		"111111",
		"fire_alarm",
	}

	if host := beego.AppConfig.String("dbhost"); host != "" {
		config.Host = host
	}

	if port, err := beego.AppConfig.Int("dbport"); err == nil {
		config.Port = port
	}

	if user := beego.AppConfig.String("dbuser"); user != "" {
		config.User = user
	}

	if pass := beego.AppConfig.String("dbpass"); pass != "" {
		config.Pass = pass
	}

	if dbName := beego.AppConfig.String("dbname"); dbName != "" {
		config.DbName = dbName
	}

	return config
}

func OrmInit() {
	config := GetDbConfig()
	orm.RegisterDriver("mysql", orm.DRMySQL)
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		config.User,
		config.Pass,
		config.Host,
		config.Port,
		config.DbName,
	)
	orm.Debug = false
	name := "default"
	orm.RegisterDataBase(name, "mysql", dsn)

	//  自动建表
	//force := false
	//verbose := true
	//err:= orm.RunSyncdb(name,force,verbose)
	//if err != nil {
	//	fmt.Println(err)
	//}
}

package models

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/structs"

	"github.com/astaxie/beego/orm"
)

const (
	AlarmStatusOn  = 1 // 进行中
	AlarmStatusOff = 2 // 已停止
)

func init() {
	orm.RegisterModel(new(Alarm))
}

// AddAlarm insert a new Alarm into database and returns
// last inserted Id on success.
func AddAlarm(m *Alarm) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetAlarmById retrieves Alarm by Id. Returns error if
// Id doesn't exist
func GetAlarmById(id int64) (v *Alarm, err error) {
	o := orm.NewOrm()
	v = &Alarm{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

func GetAlarmByCode(code string) (v *Alarm, err error) {
	o := orm.NewOrm()
	v = &Alarm{Code: code}

	if err = o.Read(v, "Code"); err == nil {
		return v, nil
	}
	return nil, err
}

func GetAlarmByStatus(status int) (v *Alarm, err error) {
	o := orm.NewOrm()
	v = &Alarm{Status: status}

	if err = o.Read(v, "Status"); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllAlarm retrieves all Alarm matches certain condition. Returns empty list if
// no records exist
func GetAllAlarm(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Alarm))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []Alarm
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateAlarm updates Alarm by Id and returns error if
// the record to be updated doesn't exist
func UpdateAlarmById(m *Alarm) (err error) {
	o := orm.NewOrm()
	v := Alarm{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

func InsertOrUpdateAlarmByCode(m *Alarm) (isUpdate bool, err error) {
	o := orm.NewOrm()
	v := Alarm{Code: m.Code}
	if err = o.Read(&v, "Code"); err == nil {
		isUpdate = true

		v.Status = 1 // 默认值
		v.Point = m.Point
		v.Location = m.Location
		v.PicHash = m.PicHash
		v.PicUrl = m.PicUrl
		v.Time = m.Time
		v.JsonData = m.JsonData

		var num int64
		if num, err = o.Update(&v); err == nil {
			isUpdate = num > 0
		}
	} else if err == orm.ErrNoRows {
		isUpdate = false

		m.Status = 1 // 默认值
		if id, err := AddAlarm(m); err == nil {
			fmt.Println("new record id:", id)
		}
	}

	return isUpdate, err
}

// DeleteAlarm deletes Alarm by Id and returns error if
// the record to be deleted doesn't exist
func DeleteAlarm(id int64) (err error) {
	o := orm.NewOrm()
	v := Alarm{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Alarm{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}

func GetAlarmsForKml() ([]structs.Alarm, error) {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	fields = strings.Split("Id,Type,Source,Code,Name,Time,Point,Location,PicUrl,PicHash,Status", ",")
	// limit: 10 (default is 10)
	limit = 10
	// offset: 0 (default is 0)
	offset = 0
	// sortby: col1,col2
	sortby = []string{"Id"}
	// order: desc,asc
	order = []string{"desc"}

	// query: k:v,k:v
	query["Status"] = fmt.Sprintf("%d", AlarmStatusOn)

	mapFieldValues, err := GetAllAlarm(query, fields, sortby, order, offset, limit)
	var alarmViews []structs.Alarm

	for _, vMap := range mapFieldValues {
		//fmt.Printf("alarm.vMap.Type = %+v\n", reflect.TypeOf(vMap))
		alarm := Alarm{}
		xMap := vMap.(map[string]interface{})
		elem := reflect.ValueOf(&alarm).Elem() // 必须传入指针才可修改vv字段值
		for f, val := range xMap {
			elemVal := elem.FieldByName(f)
			if elemVal.IsValid() {
				elemVal.Set(reflect.ValueOf(val))
			}
		}

		reg := regexp.MustCompile(`(?:\r?\n(?:\r?\n)*|,\s*)`)
		arr := reg.Split(alarm.Point, -1)
		lat, _ := strconv.ParseFloat(arr[1], 32)
		lng, _ := strconv.ParseFloat(arr[0], 32)
		alarmViews = append(alarmViews, structs.Alarm{
			Id:            alarm.Id,
			Name:          alarm.Name,
			FireLongitude: lng,
			FireLatitude:  lat,
			AlarmTime:     alarm.Time.Format(libs.FMT_YYYYMMDD_hhmmss_T),
			Images: []string{
				alarm.PicUrl,
			},
		})
	}

	return alarmViews, err
}

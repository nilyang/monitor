package models

import "time"

//--------------------------models----------------------//

type User struct {
	Id        int64     `json:"id"`
	Username  string    `json:"username"`
	Realname  string    `json:"realname"`
	Password  string    `json:"password"`
	IsLead    int       `json:"is_lead"` // 是否队长
	Role      string    `json:"role"`
	Profile   *Profile  `orm:"rel(one)" json:"profile"`
	Device    *Device   `orm:"rel(one)" json:"device"`
	Group     *Group    `orm:"rel(one)" json:"group"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
}

type Group struct {
	Id        int64  `json:"id"`
	GroupName string `json:"group_name"` // 组名
	LeadUid   int64  `json:"lead_uid"`   // 组长
}

// 关联资料表
type Profile struct {
	Id      int64  `json:"id"`
	UserId  int64  `json:"user_id"`
	Gender  string `json:"gender"`
	Age     int    `json:"age"`
	Address string `json:"address"`
	Email   string `json:"email"`
	Phone   string `json:"phone"` // 电话
	Avatar  string `json:"avatar"`
}

// 角色表
type Role struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

// 权限表
type RoleAuth struct {
	RoleId   int64  `json:"role_id"`
	Resource string `json:"resource"`
}

type Device struct {
	Id         int64  `json:"id"`
	DeviceCode string `json:"device_code"` // 设备编码
	DeviceName string `json:"device_name"` // 设备名称
}

// 灾情警报位置记录
type Alarm struct {
	Id       int64     `orm:"auto" json:"id"`
	Type     string    `json:"type"`                      // zai|ren|che
	Source   string    `json:"source"`                    // moniter|watcher
	Code     string    `json:"code"`                      // 设备编号(编码)
	Name     string    `json:"name"`                      // 设备名(云台名)
	Time     time.Time `json:"time"`                      // refresh|occur time
	Point    string    `orm:"type(text)" json:"point"`    // 点
	Location string    `orm:"type(text)" json:"location"` // 点|矩形对角位置...等...
	PicUrl   string    `json:"pic_url"`
	PicHash  string    `json:"pic_hash"`
	Status   int       `json:"status"`
	JsonData string    `json:"json_data"` // 原始信息 JSON

	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	//DeletedAt time.Time `orm:"type(datetime)" json:"deleted_at"`
}

// gps 位置记录
type GpsReport struct {
	Id       int64     `orm:"auto" json:"id"`
	Type     string    `json:"type"`                      // zai|ren|che
	Source   string    `json:"source"`                    // moniter|watcher
	Code     string    `json:"code"`                      // 设备编号(编码)
	Name     string    `json:"name"`                      // 人名、设备名
	Time     time.Time `json:"time"`                      // refresh|occur time
	Point    string    `orm:"type(text)" json:"point"`    // 点
	Location string    `orm:"type(text)" json:"location"` // 点|矩形对角位置...等...
	PicUrl   string    `json:"pic_url"`
	PicHash  string    `json:"pic_hash"`
	Status   int       `json:"status"`
	JsonData string    `json:"json_data"` // 原始信息 JSON

	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	//DeletedAt time.Time `orm:"type(datetime)" json:"deleted_at"`
}

//-------------------structs---------------------------//

// 展示用
type UserInfo struct {
	Uid      int64  `json:"id"`
	Username string `json:"username"`
	Realname string `json:"realname"`
	Password string `json:"-"`
	Role     string `json:"role"`
	IsLead   int    `json:"is_lead"`
	Profile
	Device
	Group
}

type UserData struct {
	Uid      int64   `json:"id"`
	Username string  `json:"username"`
	Password string  `json:"-"`
	IsLead   int     `json:"is_lead"` // 是否队长
	Role     string  `json:"role"`
	Profile  Profile `json:"profile"`
	Device   Device  `json:"device"`
	Group    Group   `json:"group"`
}

type UserForm struct {
	Username string `json:"username"`
	Realname string `json:"realname"`
	IsLead   int    `json:"is_lead"` // 是否队长
	DeviceId int64  `json:"device_id"`
	GroupId  int64  `json:"group_id"`
	// profile
	Gender string `json:"gender"`
	Age    int    `json:"age"`
	Phone  string `json:"phone"`
	// Device
}

func ConvertUserToInfo(uu *User) *UserInfo {
	info := UserInfo{}
	info.Uid = uu.Id
	info.Role = uu.Role
	info.IsLead = uu.IsLead
	info.Username = uu.Username
	info.Realname = uu.Realname
	info.Device = *uu.Device
	info.Profile = *uu.Profile
	info.Group = *uu.Group
	return &info
}

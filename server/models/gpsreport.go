package models

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"

	"github.com/astaxie/beego/orm"
)

const (
	GpsReportStatusOn  = 1 // 进行中
	GpsReportStatusOff = 2 // 已停止
)

func init() {
	orm.RegisterModel(new(GpsReport))
}

// AddGpsReport insert a new GpsReport into database and returns
// last inserted Id on success.
func AddGpsReport(m *GpsReport) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetGpsReportById retrieves GpsReport by Id. Returns error if
// Id doesn't exist
func GetGpsReportById(id int64) (v *GpsReport, err error) {
	o := orm.NewOrm()
	v = &GpsReport{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

func GetGpsReportByCode(code string) (v *GpsReport, err error) {
	o := orm.NewOrm()
	v = &GpsReport{Code: code}

	if err = o.Read(v, "Code"); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllGpsReport retrieves all GpsReport matches certain condition. Returns empty list if
// no records exist
func GetAllGpsReport(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(GpsReport))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []GpsReport
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateGpsReport updates GpsReport by Id and returns error if
// the record to be updated doesn't exist
func UpdateGpsReportById(m *GpsReport) (err error) {
	o := orm.NewOrm()
	v := GpsReport{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

func InsertOrUpdateGpsReportByCode(m *GpsReport) (isUpdate bool, err error) {
	o := orm.NewOrm()
	v := GpsReport{Code: m.Code}
	// ascertain id exists in the database
	if err = o.Read(&v, "Code"); err == nil {
		isUpdate = true
		v.Status = 1 // 默认值
		v.JsonData = m.JsonData
		v.Location = m.Location
		v.Point = m.Point
		v.Time = m.Time
		v.PicUrl = m.PicUrl
		v.PicHash = m.PicHash

		var num int64
		if num, err = o.Update(&v); err == nil {
			isUpdate = num > 0
		}
	} else if err == orm.ErrNoRows {
		isUpdate = false
		m.Status = 1 // 默认值
		if id, err := AddGpsReport(m); err == nil {
			fmt.Println("new record id:", id)
		}
	}

	return isUpdate, err
}

// DeleteGpsReport deletes GpsReport by Id and returns error if
// the record to be deleted doesn't exist
func DeleteGpsReport(id int64) (err error) {
	o := orm.NewOrm()
	v := GpsReport{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&GpsReport{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}

// 查询gps信息到KML
func GetPositionsForKml(pType int) ([]structs.PositionView, error) {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64
	var offset int64

	// fields: col1,col2,entity.col3
	// fields = []string{} // all

	// limit: -1 (default is -1, then limit 1000)
	limit = -1
	// offset: 0 (default is 0)
	offset = 0
	// sortby: col1,col2
	//sortby = []string{"Id"}
	// order: desc,asc
	//order = []string{"desc"}

	// query: k:v,k:v
	query["Status"] = fmt.Sprintf("%d", GpsReportStatusOn)
	if pType == 1 {
		query["Type"] = "ren"
	} else if pType == 2 {
		query["Type"] = "che"
	}

	var psArr []structs.PositionView
	gpsArr, err := GetAllGpsReport(query, fields, sortby, order, offset, limit)

	for _, vMap := range gpsArr {
		vv := GpsReport{}
		//fmt.Printf("vMap-Type= %v\n", reflect.TypeOf(vMap))
		if reflect.TypeOf(vMap) == reflect.TypeOf(vv) {
			vv = vMap.(GpsReport)
		} else {
			//vv 赋值
			xMap := vMap.(map[string]interface{})
			elem := reflect.ValueOf(&vv).Elem() // 必须传入指针才可修改vv字段值
			for f, val := range xMap {
				elemVal := elem.FieldByName(f)
				if elemVal.IsValid() {
					elemVal.Set(reflect.ValueOf(val))
				}
			}
		}

		reg := regexp.MustCompile(`(?:\r?\n(?:\r?\n)*|,\s*)`)
		arr := reg.Split(vv.Point, -1)

		lng, _ := strconv.ParseFloat(arr[0], 32)
		lat, _ := strconv.ParseFloat(arr[1], 32)
		ps := structs.Position{}
		ps.Id = fmt.Sprintf("%d", vv.Id)
		ps.Name = vv.Name
		ps.Latitude = lat
		ps.Longitude = lng
		ps.Altitude = 0
		if vv.Type == "ren" {
			ps.Type = 1
		} else if vv.Type == "che" {
			ps.Type = 2
		}
		ps.WriteTime = vv.Time.Format(libs.FMT_YYYYMMDD_hhmmss_T)
		ps.GpsNumber = vv.Code

		psArr = append(psArr, utils.PositionToView(ps))
	}

	return psArr, err
}

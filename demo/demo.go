package main

import (
	"fmt"
	"log"
	"os"
)

const x, y int = 1, 2
const s = "Hello, World!"

func WriteFile() {
	const x string = "xxx"
	fmt.Println(s)
	test()
	writefile()
}

func test() {
	s, y := "hello", 20
	println(&s, y)
	println(7 / 30)
}

func writefile() {
	f, err := os.OpenFile("./test.log", os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Println(err)
		return
	}

	var text []byte
	text = []byte("001,333")
	f.Write(text)
	defer f.Close()
	log.Println("write ok")

}

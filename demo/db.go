package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Task struct {
	gorm.Model
	Code     string
	Location string
}

func DbConnect() {
	dsn := "homestead:secret@tcp(localhost:33060)/fire_alarm?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		fmt.Println("[ERROR] Database connect failed: ", err)
	}
	defer db.Close()

	db.AutoMigrate(&Task{})

	db.Create(&Task{Code: "001", Location: "233,444.55"})
}

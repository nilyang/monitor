package main

import "fmt"

type Foo struct {
	Name string
	Age  int
}

func basic() {
	var x interface{}
	x = Foo{}
	fmt.Println(x.(Foo).Name)
}

func add(n, m int) (sum int) {
	sum = n + m
	return
}

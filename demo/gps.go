package main

import (
	"fmt"
	"math"

	"gitee.com/nilyang/monitor/utils"
)

func gps() {

	lat1 := 29.67255462159139 // 29°40′21.19663772900964″
	lat2 := 29.67210085203518 // 29°40′19.56306732664359″
	lng1 := 106.7130842482397 // 106°42′47.10329366290466″
	lng2 := 106.7123329154576 // 106°42′44.39849564733890″
	P1 := utils.Point{Lat: lat1, Lng: lng2}
	P2 := utils.Point{Lat: lat2, Lng: lng1}
	degLat1 := utils.DegreePoint{}
	degLat1.MakeDegree(lat1)
	degLat2 := utils.DegreePoint{}
	degLat2.MakeDegree(lat2)
	degLng1 := utils.DegreePoint{}
	degLng1.MakeDegree(lng1)
	degLng2 := utils.DegreePoint{}
	degLng2.MakeDegree(lng2)

	fmt.Println(utils.DegreeFormat(lat1))
	fmt.Println(utils.DegreeFormat(lat2))
	fmt.Println(utils.DegreeFormat(lng1))
	fmt.Println(utils.DegreeFormat(lng2))

	fmt.Println("lat1 - lat2: ", utils.DegreeDistanceLat(degLat2.Second, degLat1.Second))
	fmt.Println("lng1 - lng2: ", utils.DegreeDistanceLng(degLng2.Second, degLng1.Second, lat1))
	// 勾股
	w := utils.DegreeDistanceLng(degLng2.Second, degLng1.Second, lat1) / 2
	h := utils.DegreeDistanceLat(degLat2.Second, degLat1.Second) / 2
	c := math.Sqrt(math.Pow(w*2, 2) + math.Pow(h*2, 2))
	fmt.Println("p1 <-distance-> p2 : ", c)
	fmt.Println("")

	// 制造矩形

	p1, p2 := utils.MakeSquare(lat1, lng2, w, h)
	fmt.Println(p1)
	fmt.Println(p2)
	fmt.Println(utils.GetDistance(p1, p2))
	fmt.Println("")
	p3, p4 := utils.MakeSquare(p2.Lat, p2.Lng, w*2, h*2)
	fmt.Println(p3)
	fmt.Println(p4)
	fmt.Println("p3<-->p4: ", utils.GetDistance(p3, p4))
	fmt.Println("p1<-->p4: ", utils.GetDistance(p1, p4))
	fmt.Println("P1<-->P2: ", utils.GetDistance(P1, P2))
	fmt.Println("p1.lat: ", utils.DegreeFormat(p1.Lat))
	fmt.Println("p1.lng: ", utils.DegreeFormat(p1.Lng))
	fmt.Println("P1.lat: ", utils.DegreeFormat(P1.Lat))
	fmt.Println("P1.lng: ", utils.DegreeFormat(P1.Lng))

	fmt.Println("")
	fmt.Println("++++++++++++++++++")
	lat := 29.92278611111111
	lng := 106.60823333333332
	pp1, pp2 := utils.MakeSquare(lat, lng, 150, 100)
	fmt.Println("squre pp1: ", pp1)
	fmt.Println("squre pp2: ", pp2)
	fmt.Println("Important: pp1 <--distance-> pp2: ", utils.GetDistance(pp1, pp2))
	fmt.Println("++++++++++++++++++")
	fmt.Println("")

	/*
		    p1
		    p3
		   (lat1,lng2)---------.
			|        |         |
			|        |         |
			|--------p2--------|
			|        |         |
			|        |         |
			'-----------------(lat2,lng1)
		                       p4
	*/
	point := utils.MakePoint(lat1, lng1)
	distance := point.GetDistance(lat2, lng2)
	fmt.Println("distance = ", distance)

	Lat := 29.92278611111111
	Lng := 106.60823333333332
	fmt.Println(utils.DegreeFormat(Lat))
	fmt.Println(utils.DegreeFormat(Lng))

	fmt.Println("")

	fmt.Println("lat 1 second meters: ", utils.DegreeDistanceLat(1, 2))
	fmt.Println("lng 1 second meters: ", utils.DegreeDistanceLng(1, 2, Lat))
	fmt.Println("lat 1 munite meters: ", utils.DegreeDistanceLat(0, 60))
	fmt.Println("lng 1 munite meters: ", utils.DegreeDistanceLng(0, 60, Lat))

	//fmt.Println("\n-----------------------------\n")
	//point.MakeSquare(100,100)
}

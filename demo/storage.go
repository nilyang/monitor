package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"gitee.com/nilyang/monitor/utils"
	"github.com/astaxie/beego/httplib"
)

func storage() {
	fmt.Println(os.TempDir())
	url := saveFile()
	deleteFile(url)
}

func demo() {
	a := [...]string{"a", "b", "c", "d"}
	for i := range a {
		fmt.Println("Array item", i, "is", a[i])
	}

	fmt.Printf("\n%b %b\n", (1 << 10), 2<<2)

	s := make([]byte, 5)
	fmt.Println(len(s))
	fmt.Println(cap(s))
	s = s[2:4]
	fmt.Printf("\nlen(s) = %d, cap(s) = %d\n", len(s), cap(s))
}

func saveFile() string {
	f, err := utils.StorageGetFile("/dir/assign")
	if err != nil {
		println(err.Error())
		return ""
	}

	log.Println(f.Fid)
	log.Println(f.Url)
	log.Println(f.PublicUrl)

	pwd, err := os.Getwd()
	file := path.Join(pwd, "demo/fly3.png")
	//file := path.Join(pwd, "fly3.png")
	url := fmt.Sprintf("http://%s/%s", strings.TrimLeft(f.Url, "http://"), f.Fid)
	log.Println(file)
	log.Println(url)
	size, err := utils.StoragePutFile(file, url)
	if err != nil {
		log.Println(err.Error())
		return ""
	}
	f.Size = size
	log.Printf("%v", f)
	return url
}

func deleteFile(url string) {
	//url := "http://localhost:8333/2,760d65d363"
	req := httplib.Delete(url)
	log.Println(req.String())
}

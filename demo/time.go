package main

import (
	"fmt"
	"time"

	"gitee.com/nilyang/monitor/libs"
)

func timex() {
	t := "2017-11-09T10:11:35"
	tt, _ := time.Parse("2006-01-02T15:04:05", t)

	fmt.Println(time.Now().Format("2006-01-02T15:04:05"))
	fmt.Println(tt.Format("2006-01-02 15:04:05"))

	// 2006-01-02T15:04:05 --> 2006-01-02 15:04:05
	x, _ := libs.TimeFormatFromT(t)
	fmt.Println(x)
}

package main

import (
	"os"
	"time"
	//"flag"
	"fmt"
	"io"
	"path/filepath"
	"strings"
)

const (
	IsDirectory = iota
	IsRegular
	IsSymlink
)

type sysFile struct {
	fType  int
	fName  string
	fLink  string
	fSize  int64
	fMtime time.Time
	fPerm  os.FileMode
}

type F struct {
	files []*sysFile
}

func (self *F) visit(path string, f os.FileInfo, err error) error {
	if f == nil {
		return err
	}

	var tp int
	if f.IsDir() {
		tp = IsDirectory
	} else if (f.Mode() & os.ModeSymlink) > 0 {
		tp = IsSymlink
	} else {
		tp = IsRegular
	}
	inoFile := &sysFile{
		fName:  path,
		fType:  tp,
		fPerm:  f.Mode(),
		fMtime: f.ModTime(),
		fSize:  f.Size(),
	}

	self.files = append(self.files, inoFile)
	return nil
}

func mainFunc() {
	srcDir := ""
	distDir := ""

	src := F{
		files: make([]*sysFile, 0),
	}
	err := filepath.Walk(srcDir, func(path string, info os.FileInfo, err error) error {
		return src.visit(path, info, err)
	})

	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}

	dist := F{
		files: make([]*sysFile, 0),
	}
	err = filepath.Walk(distDir, func(path string, info os.FileInfo, err error) error {
		return dist.visit(path, info, err)
	})

	if err != nil {
		fmt.Printf("filepath.wak() returnd %v\n", err)
	}

	//TODO finish here
	//for  v : range.files {
	//	if (com.IsFils)
	//}

}

// 拷贝文件
func copyFile(srcFile, destFile string) error {
	file, err := os.Open(srcFile)
	if err != nil {
		return err
	}
	defer file.Close()

	dest, err := os.Create(destFile)
	if err != nil {
		return err
	}
	defer dest.Close()

	io.Copy(dest, file)

	return nil
}

// 获取要生成的文件（夹）的绝对路径名称
// src 原基目录
// dest 目标基地址
// fileName 文件（简单）名称
func getFinalFilePath(src, dest, fileName string) string {
	return strings.Replace(fileName, src, dest, 1)
}

// 处理文件（非目录）
func handleFile(src, dest, fileName string) error {
	return copyFile(fileName, getFinalFilePath(src, dest, fileName))
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

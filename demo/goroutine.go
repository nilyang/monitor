package main

import (
	"fmt"
	"runtime"
	"time"
)

func loop() {
	for i := 0; i < 10; i++ {
		fmt.Printf("%d ", i)
	}
}

func goRoutine() {
	go loop()
	loop()
	time.Sleep(time.Second)
}

func goChannelBlocked() {
	var messages chan string = make(chan string)
	go func(message string) {
		messages <- message // 存消息
	}("Ping!")

	fmt.Println(<-messages)
}

func goChannelUnblocked() {
	var complete chan int = make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			fmt.Printf("%d ", i)
		}

		complete <- 0 // 执行完毕了，发个消息
	}()

	<-complete // 阻塞，直到线程跑完，取消息 main在此阻塞
	fmt.Println("\nComplete")
}

func deadBlock() {
	ch := make(chan int)
	<-ch // 1流入信道，堵塞当前线, 没人取走数据信道不会打开

	//报错
	// fatal error: all goroutines are asleep - deadlock!

}

//非缓冲信道上如果发生了流入无流出，或者流出无流入，也就导致了死锁。
func deadBlock2() {
	c, quit := make(chan int), make(chan int)

	go func() {
		c <- 1    // c通道的数据没有被其他goroutine读取走，堵塞当前goroutine
		quit <- 0 // quit始终没有办法写入数据
	}()

	<-quit // quit 等待数据的写
}

// 是否所有不成对向信道存取数据的情况都是死锁?
func nonDeadBlock() {
	/**
	程序正常退出了，很简单，并不是我们那个总结不起作用了，
	还是因为一个让人很囧的原因，main又没等待其它goroutine，自己先跑完了，
	所以没有数据流入c信道，一共执行了一个goroutine,
	并且没有发生阻塞，所以没有死锁错误
	*/
	c := make(chan int)

	go func() {
		c <- 1
	}()

}

/*
	那么死锁的解决办法呢？
	最简单的，把没取走的数据取走，没放入的数据放入，
	因为无缓冲信道不能承载数据，那么就赶紧拿走！

	1.针对 deadBlock2() 等死锁解决办法就可以 将c通道内的数据拿走就行 <-c
	2.另外一个解决办法是缓冲通道（即设置c有一个数据的缓冲大小）：见 unBlocked2()
*/
func deadBlock3() {
	c, quit := make(chan int), make(chan int)
	go func() {
		c <- 1
		quit <- 0
	}()

	<-c //取走c的数据！
	<-quit
}

//  设置c有一个数据的缓冲大小
func nonDeadBlock2() {
	c, quit := make(chan int, 1), make(chan int)
	go func() {
		c <- 1
		quit <- 0
	}()

	<-quit
}

// 无缓冲信道
func unBufferedChannel() {
	var ch chan int = make(chan int)

	var func_routine = func(id int) {
		ch <- id
	}

	// 开启5个routine
	for i := 0; i < 5; i++ {
		go func_routine(i)
	}

	// 取出信道中的数据
	for i := 0; i < 5; i++ {
		fmt.Print(<-ch)
	}
}

// 缓冲信道
func bufferedChannel() {
	// 1. 缓冲信道是先进先出的，我们可以把缓冲信道看作为一个线程安全的队列
	ch := make(chan int, 3)
	ch <- 1
	ch <- 2
	ch <- 3
	//ch <- 4 //多一个就会阻塞，等待消费（而由于没有消费，则死锁）
	fmt.Printf("%d ", <-ch) // 1
	fmt.Printf("%d ", <-ch) // 2
	fmt.Printf("%d ", <-ch) // 3

	fmt.Println("\n----\n")

	//2. Go语言允许我们使用range来读取信道
	ch = make(chan int, 4)
	ch <- 1
	ch <- 2
	ch <- 3
	ch <- 4

	//for v := range ch  {
	//	fmt.Println(v)
	//}

	// but,   range不等到信道关闭是不会结束读取的
	// 也就是如果 缓冲信道干涸了，那么range就会阻塞当前goroutine, 所以会死锁
	// 比较容易想到等方法是，判定边界，避免多读就跳出，不用让range等
	for v := range ch {
		fmt.Printf("%d ", v)
		// 如果现有数据量为0，跳出循环
		if len(ch) <= 0 {
			break
		}
	}
	fmt.Println("\n----\n")

	//3. 另外一个方式是显式地关闭信道：
	ch = make(chan int, 3)
	ch <- 5
	ch <- 6
	ch <- 7
	// 被关闭的信道会禁止数据流入, 是只读的。
	// 我们仍然可以从关闭的信道中取出数据，但是不能再写入数据了。
	close(ch)

	for v := range ch {
		fmt.Printf("%d ", v)
	}
}

func singleNoBufferedChannel() {
	// no buffer
	var quit chan int // 只开一个信道
	var foo = func(id int) {
		fmt.Println(id)
	}

	count := 1000
	quit = make(chan int) // 无缓冲

	for i := 0; i < count; i++ {
		go foo(i)
	}

	for i := 0; i < count; i++ {
		<-quit
	}
}

func multiBufferedChannel() {
	// 把信道换成缓冲1000的
	count := 1000
	var quit chan int = make(chan int, count) // 只开启1个信道

	var foo = func(id int) {
		fmt.Println(id)
		quit <- 0 // OK, finished
	}

	for i := 0; i < count; i++ {
		go foo(i)
	}
	for i := 0; i < count; i++ {
		<-quit
	}
}

/*
缓冲与非缓冲channel，
其实区别仅仅在于一个是缓冲的，一个是非缓冲的。
对于这个场景而言，两者都能完成任务, 都是可以的。
无缓冲的信道是一批数据一个一个的「流进流出」
缓冲信道则是一个一个存储，然后一起流出去
*/

func multiCore() {
	var quit chan int = make(chan int)
	var loop = func() {
		for i := 0; i < 100; i++ { // 为了观察多跑一下任务
			runtime.Gosched() //显式地出让CPU时间
			fmt.Printf("%d ", i)
		}
		quit <- 0
	}

	runtime.GOMAXPROCS(2) // 最多使用2个核
	go loop()
	go loop()

	for i := 0; i < 2; i++ {
		<-quit
	}
}

package main

import (
	"log"
	"testing"
)

func Test_Producer(t *testing.T) {
	if err := publish(*exchangeName, *routingKey, *body, *reliable); err != nil {
		log.Fatalf("%s", err)
	}
	log.Printf("published %dB OK", len(*body))
}

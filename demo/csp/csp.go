package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"runtime"
	"sync"
	"time"
)

func test4() {
	var wg sync.WaitGroup
	var i int
	var file string

	for i, file = range os.Args[1:] {
		fmt.Println(i, file)
		wg.Add(1)
		go func(filename string) {
			compress(filename)
			wg.Done()
		}(file)
	}
	wg.Wait()
	fmt.Printf("Compressed %d files\n", i+1)
}

func test3() {
	//compress
	for _, file := range os.Args[1:] {
		compress(file)
	}
}

func compress(filename string) error {
	in, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(filename + ".gz")
	if err != nil {
		return err
	}
	defer out.Close()

	gzout := gzip.NewWriter(out)
	_, err = io.Copy(gzout, in)
	gzout.Close()

	return err
}

func test2() {
	fmt.Println("Outside a goroutine.")
	go func() {
		fmt.Println("Inside a goroutine")
	}()
	fmt.Println("Outside again.")

	runtime.Gosched()
}

func test1() {
	go echo(os.Stdin, os.Stdout)
	time.Sleep(30 * time.Second)
	fmt.Println("Timed out.")
	os.Exit(0)
}
func echo(in io.Reader, out io.Writer) {
	io.Copy(out, in)
}

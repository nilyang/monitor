package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"

	"gitee.com/nilyang/monitor/libs"

	"github.com/fsnotify/fsnotify"
)

func checkFile() {
	root_dir, err := os.Getwd()
	watchdir := root_dir + "\\demo\\csp\\test"
	log.Println(watchdir)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Println("err watch:", err)
		return
	}
	defer watcher.Close()

	lock := new(sync.Mutex)
	done := make(chan bool)
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				log.Println("event:", event)

				lock.Lock()
				//getFileData(event.Name)
				libs.FileGetContents(event.Name)
				lock.Unlock()
			case err := <-watcher.Errors:
				log.Println("error: ", err)
			}
		}
	}()

	if err := watcher.Add(watchdir); err != nil {
		fmt.Println("err add watch:", err)
	}

	<-done
}

func getFileData(fname string) {
	for {
		ok, err := PathExists(fname)
		if ok {
			fmt.Println("OK")
		} else {
			fmt.Println("Not Exists!")
			return
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		finfo, err := ioutil.ReadFile(fname)
		fmt.Println(finfo)
		if err == nil {
			fmt.Println(string(finfo))
			return
		}
	}
}
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

The main function loops over all the files you supply on the command line, generating
statistics for each as it goes. What you expect, when you run the preceding code, is for
the tool to read text files and print out a list of the words that it finds. Let’s try it on a
single file.
In this revised version, the words struct declares an anonymous field referencing
sync.Mutex, basically granting the words.Lock and words.Unlock methods. This is a
common way of exposing a lock on a struct. (You used these methods when looping
over the words at the end of main.)
package main

import (
	"fmt"
	"testing"
	"time"
)

func TestSend(t *testing.T) {

	msg := make(chan string)
	until := time.After(5 * time.Second)

	go Send(msg)
	for {
		select {
		case m := <-msg:
			fmt.Println(m)
		case <-until:
			close(msg)
			time.Sleep(500 * time.Millisecond)
			return
		}
	}

}

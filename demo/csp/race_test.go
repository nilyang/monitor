package main

import (
	"fmt"
	"os"
	"sync"
	"testing"
)

func Test_Race(t *testing.T) {
	var wg sync.WaitGroup
	w := newWords()
	for _, f := range os.Args[1:] {
		wg.Add(1)
		go func(file string) {
			if err := tallyWords(file, w); err != nil {
				fmt.Println(err.Error())
			}
			wg.Done()
		}(f)
	}
	wg.Wait()

	fmt.Println("Words that appear more than onece:")
	////////////////////////////////////////
	//Locks and unlocks the map
	//when you iterate at the end.
	//Strictly speaking, this isn’t
	//necessary because you know
	//that this section won’t
	//happen until all files are
	//processed
	////////////////////////////////////////
	//w.Lock()
	for word, count := range w.found {
		if count > 1 {
			fmt.Printf("%s: %d\n", word, count)
		}
	}
	//w.Unlock()
}

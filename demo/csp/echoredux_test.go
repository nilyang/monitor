package main

import (
	"fmt"
	"os"
	"testing"
	"time"
)

func Test_Echoredux(t *testing.T) {
	done := time.After(30 * time.Second)
	echo := make(chan []byte)
	go readStdin(echo)
	for {
		select {
		case buf := <-echo:
			os.Stdout.Write(buf)
		case <-done:
			fmt.Println()
		}
	}
}

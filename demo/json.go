package main

import (
	"encoding/json"
	"fmt"

	"gitee.com/nilyang/monitor/server/models"
	"github.com/tidwall/gjson"
)

func jsonOpration() {
	data := []byte(`
{
  "device": {
    "code": "C-001",
    "name": "消防车"
  },
  "group": {
    "name": "消防1队"
  },
  "profile": {
    "address": "重庆市北碚区",
    "age": 30,
    "avatar": "forrest.svg",
    "email": "demo@qq.com",
    "gender": "male",
    "phone": "15088888888",
  },
  "is_lead": 1,
  "password": "111111",
  "username": "demo"
}
`)
	u := models.UserData{}

	//value :=gjson.GetManyBytes(data, "group","device", "profile", "is_lead","username","password")
	//gjson.Unmarshal([]byte(value[0].Raw), &u.Group)
	//gjson.Unmarshal([]byte(value[1].Raw), &u.Device)
	//gjson.Unmarshal([]byte(value[2].Raw), &u.Profile)
	//gjson.Unmarshal([]byte(value[3].Raw), &u.IsLead)
	//gjson.Unmarshal([]byte(value[4].Raw), &u.Username)
	//gjson.Unmarshal([]byte(value[5].Raw), &u.Password)

	if !gjson.Valid(string(data)) {
		fmt.Printf("json error")
		return
	}

	json.Unmarshal(data, &u)
	fmt.Printf("%v\n", u)

}

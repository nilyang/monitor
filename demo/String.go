package main

import (
	"fmt"
	"os"
	"unicode/utf8"
)

func String() {
	//var s string = "你好，Hello, World!"
	//fmt.Printf("%c %d",s[0], len(s))
	//fmt.Println(s[0:9],"\n", s[0:3])
	//fmt.Println("goodbye"+ s[9:])
	fmt.Println("222" == "222")

	// 字符串是不可修改
	s := "left foot"
	t := s
	s += ", right foot"
	// 这并不会导致原始的字符串值被改变，
	// 但是变量s将因为+=语句持有一个新的字符串值，
	// 但是t依然是包含原先的字符串值。
	fmt.Println(s)
	fmt.Println(t)
	//字符串是不可修改的
	//s[0] = 'L' // cannot assign to s[0]
	//不变性意味如果两个字符串共享相同的底层数据的话也是安全的，
	// 这使得复制任何长度的字符串代价是低廉的。

	// 原生字符串
	//const GoUsage = `Go is a tool for managing Go code.
	//
	//Usage:
	//	go command [arguments]
	//
	//---  ==\r`
	//fmt.Println(GoUsage)

	s = "Hello,世界"
	fmt.Println(len(s))
	fmt.Println(utf8.RuneCountInString(s))

	// 将字符转为字符串
	var path byte = os.PathSeparator
	fmt.Println(string(path))

	// rune 类型
	var chars []rune = []rune(s)
	for i := range chars {
		fmt.Printf("%c %d", chars[i], i)
	}
	fmt.Println()

	changeString()
}

func changeString() {
	str := "你好"
	// 把字符串转换成字符数组
	chars := []rune(str)
	// 输出每一个字符(rune)
	for i := range chars {
		fmt.Printf("%c", chars[i])
	}
	fmt.Println()

	// 修改一个字符
	chars[1] = '我'

	// 再通过把[]rune转换成string，从而达到修改字符串的目的
	newStr := string(chars)
	fmt.Println(newStr)

}

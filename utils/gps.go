package utils

import (
	"math"
)

const mPI float64 = math.Pi

// WGS84地理坐标系参变量
type CoordGeodetic struct {
	latitude  float64
	longitude float64
	altitude  float64
}

// 空间笛卡尔坐标系坐标点
type CoordCartesian struct {
	x, y, z float64
}

type CoordCovert struct {
}

func pow(x, y float64) float64 {
	return math.Pow(x, y)
}
func sin(x float64) float64 {
	return math.Sin(x)
}
func cos(x float64) float64 {
	return math.Cos(x)
}
func sqrt(x float64) float64 {
	return math.Sqrt(x)
}
func atan(x float64) float64 {
	return math.Atan(x)
}
func qAbs(x float64) float64 {
	return math.Abs(x)
}

//最精确的坐标转换办法 ,空间大地坐标系向空间直角坐标系的转换
//大地--->球心
func (this CoordCovert) BLHtoXYZ(posBLH CoordGeodetic) CoordCartesian {
	var a float64 = 6378137      //a为椭球的长半轴:a=6378.137km
	var b float64 = 6356752.3141 //b为椭球的短半轴:a=6356.7523141km
	var H float64 = posBLH.altitude + a
	var e float64 = sqrt(1 - pow(b, 2)/pow(a, 2)) //e为椭球的第一偏心率
	//fmt.Println("e:", e) // WGS-84 扁率
	//fmt.Println("f:",(a-b)/a)
	//fmt.Println("f':",1/298.25722363) // WGS-84 扁率 f=(a-b)/a = (6378137-6356752.3142)/6378137
	// var e float64=sqrt(0.006693421622966); //克拉索夫斯基椭球
	// var e float64=sqrt(0.006694384999588); //1975年国际椭球
	// var e float64=sqrt(0.0066943799013); //WGS-84椭球 第一偏心率
	var pos_XYZ CoordCartesian
	var m float64 = mPI / 180 //经度维度需要转换成弧度.
	var B float64 = posBLH.latitude * m
	var L float64 = posBLH.longitude * m
	var N float64 = a / sqrt(1-pow(e, 2)*pow(sin(B), 2)) //N为椭球的卯酉圈曲率半径
	pos_XYZ.x = (N + H) * cos(B) * cos(L)
	pos_XYZ.y = (N + H) * cos(B) * sin(L)
	pos_XYZ.z = (N*(1-pow(e, 2)) + H) * sin(B)
	return pos_XYZ
}

//球心--->大地
func (this CoordCovert) XYZtoBHL(posXYZ CoordCartesian) CoordGeodetic {
	var v0 float64 = posXYZ.z / sqrt(pow(posXYZ.x, 2)+pow(posXYZ.y, 2))
	var a float64 = 6378137                       //a为椭球的长半轴:a=6378.137km
	var b float64 = 6356752                       //b为椭球的短半轴:b=6356.752km
	var e float64 = sqrt(1 - pow(b, 2)/pow(a, 2)) //e为椭球的第一偏心率
	// var e float64 =sqrt(0.006693421622966); //克拉索夫斯基椭球
	// var e float64 =sqrt(0.006694384999588); //1975年国际椭球
	// var e float64 =sqrt(0.0066943799013); //WGS-84椭球
	// WGS-84椭球体，规定扁率为1/298.257223563
	// var W float64 =sqrt(1-pow(e ,2)*pow(sin(B) ,2));
	var N float64 = 0 //N为椭球的卯酉圈曲率半径
	var B1 float64 = atan(v0)
	var B2 float64 = 0
	var H float64 = 0
	for qAbs(B2-B1) > 1E-5 {
		N = a / sqrt(1-pow(e, 2)*pow(sin(B1), 2))
		H = posXYZ.z/sin(B1) - N*(1-pow(e, 2))
		B2 = atan(posXYZ.z * (N + H) / sqrt((pow(posXYZ.x, 2)+pow(posXYZ.y, 2))*(N*(1-pow(e, 2))+H)))
		B1 = B2
	}
	var m float64 = mPI / 180
	var pos_BLH CoordGeodetic
	pos_BLH.latitude = B1 / m
	pos_BLH.longitude = atan(posXYZ.y/posXYZ.x) / m
	pos_BLH.altitude = H - a
	return pos_BLH
}

//球心--->站心
func (this CoordCovert) XYZToXyz(pos_XYZ CoordCartesian, center CoordGeodetic) CoordCartesian {
	var pos_xyz, tmpXYZ CoordCartesian
	CenterXYZ := this.BLHtoXYZ(center)
	tmpXYZ.x = pos_XYZ.x - CenterXYZ.x
	tmpXYZ.y = pos_XYZ.y - CenterXYZ.y
	tmpXYZ.z = pos_XYZ.z - CenterXYZ.z
	var B0 = Rad(center.latitude)
	var L0 = Rad(center.longitude)
	pos_xyz.x = -sin(B0)*cos(L0)*tmpXYZ.x - sin(B0)*sin(L0)*tmpXYZ.y + cos(B0)*tmpXYZ.z
	pos_xyz.y = -sin(L0)*tmpXYZ.x + cos(L0)*tmpXYZ.y
	pos_xyz.z = cos(B0)*cos(L0)*tmpXYZ.x + cos(B0)*sin(L0)*tmpXYZ.y + sin(B0)*tmpXYZ.z - a
	return pos_xyz
}

//站心--->球心
func (this CoordCovert) XyzToXYZ(pos_xyz CoordCartesian, center CoordGeodetic) CoordCartesian {
	var a float64 = 6378137      //a为椭球的长半轴:a=6378.137km
	var b float64 = 6356752.3141 //b为椭球的短半轴:a=6356.7523141km
	var H0 float64 = center.altitude + a
	var e float64 = sqrt(1 - pow(b, 2)/pow(a, 2)) //e为椭球的第一偏心率
	// var e float64 =sqrt(0.006693421622966); //克拉索夫斯基椭球
	// var e float64 =sqrt(0.006694384999588); //1975年国际椭球
	// var e float64 =sqrt(0.0066943799013); //WGS-84椭球
	var B0 float64 = Rad(center.latitude) // 经纬度需要转换成弧度.
	var L0 float64 = Rad(center.longitude)
	var W float64 = sqrt(1 - pow(e, 2)*pow(sin(B0), 2))
	var N0 float64 = a / W //N为椭球的卯酉圈曲率半径
	var pos_XYZ CoordCartesian
	pos_XYZ.x = (N0+H0)*cos(B0)*cos(L0) - sin(B0)*cos(L0)*pos_xyz.x - sin(L0)*pos_xyz.y + cos(B0)*cos(L0)*pos_xyz.z
	pos_XYZ.y = (N0+H0)*cos(B0)*sin(L0) - sin(B0)*sin(L0)*pos_xyz.x - cos(L0)*pos_xyz.y + cos(B0)*sin(L0)*pos_xyz.z
	pos_XYZ.z = (N0*(1-pow(e, 2))+H0)*sin(B0) - cos(B0)*pos_xyz.x + sin(B0)*pos_xyz.z
	return pos_XYZ
}

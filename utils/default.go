package utils

import (
	"math/rand"
	"time"
)

const SECRET_KEY string = "igcUKOBLaxjRiQLABiFUL0ObzE7F0BFlr4BL8Mf1HsaWlMzdjdGGhjgPKXoYqai9"
const AES_KEY string = "sfA0-=zF%d3&fGfl"
const STORAGE_URL string = "http://localhost:9333"
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// RandStringBytesMaskImprSrc
func RandomString(n int) string {
	var src = rand.NewSource(time.Now().UnixNano())

	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func JsonError(data interface{}, params ...interface{}) map[string]interface{} {
	return JsonResult(50000, data)
}
func JsonSuccess(data interface{}, params ...interface{}) map[string]interface{} {
	return JsonResult(20000, data)
}

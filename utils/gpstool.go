package utils

import (
	"fmt"
	"math"
	"regexp"
	"strconv"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/structs"
)

// gps 位置距离计算

// 地球半径
const EarthRadius = 6378.137

// GE坐标点解析正则
const RegGEPointStr string = `(?P<DegLat>\d+)\s*[°度]\s*(?P<MinLat>\d+)\s*['′分]\s*(?P<SecLat>\d+(?:.\d+))\s*[″〃"秒]\s*北\s*\r?\n(?P<DegLng>\d+)\s*[°]\s*(?P<MinLng>\d+)\s*[′']\s*(?P<SecLng>\d+(?:.\d+))\s*[″〃"]\s*东`

type Point struct {
	Lng float64 `json:"lng"`
	Lat float64 `json:"lat"`
	Alt float64 `json:"alt"`
}

func (p Point) ToString() string {
	return fmt.Sprintf("%.16f,%.16f", p.Lng, p.Lat)
}

func (p Point) ToKmlPointString() string {
	return fmt.Sprintf("%.16f,%.16f,%.0f", p.Lng, p.Lat, p.Alt)
}

func (p Point) GetDistance(lat2, lng2 float64) float64 {
	radLat1 := Rad(p.Lat)
	radLat2 := Rad(lat2)
	a := radLat1 - radLat2
	b := Rad(p.Lng) - Rad(lng2)
	s := 2 * math.Asin(math.Sqrt(math.Pow(math.Sin(a/2), 2)+math.Cos(radLat1)*math.Cos(radLat2)*math.Pow(math.Sin(b/2), 2)))
	s = s * EarthRadius * 1000 // 单位米

	return s
}

func (p Point) MakeSquare(width, height float64) (Point, Point) {
	var cc CoordCovert
	var geo CoordGeodetic // 设置位置原点
	geo.latitude = p.Lat
	geo.longitude = p.Lng
	geo.altitude = 0

	// 已知p，找 p1,p2
	/*
		   (p1)----------------.
			|        |         |
			|        |         |
			|--------p---------|
			|        |         |
			|        |         |
			'-----------------(p2)
	*/
	var (
		p1, p2   CoordGeodetic
		pp1, pp2 CoordCartesian
	)

	// 球心位置
	pp := cc.BLHtoXYZ(geo)
	// 站心位置
	ppp := cc.XYZToXyz(pp, geo)

	// 站心位移，展开以站心为重心的矩形坐标
	// 参考文档 https://wenku.baidu.com/view/c906011d227916888486d7e1.html

	// 画矩形 中心点移位变换 ：站心北移位 + 左移位
	pp1.x = ppp.x + width  // 左移位
	pp1.y = ppp.y - height // 上移位

	pp2.x = ppp.x - width  // 右移位
	pp2.y = ppp.y + height // 下移位

	// p1,p2 站心转球心
	tmp1 := cc.XyzToXYZ(pp1, geo)
	tmp2 := cc.XyzToXYZ(pp2, geo)

	// 球心转大地
	p1 = cc.XYZtoBHL(tmp1)
	p2 = cc.XYZtoBHL(tmp2)

	//fmt.Println(pp,cc.XYZtoBHL(pp))
	//fmt.Println(pp1)
	//fmt.Println(pp2)
	fmt.Printf("p1: %10.16f, %10.16f, %10.16f\n", p1.latitude, p1.longitude, p1.altitude)
	fmt.Printf("p2: %10.16f, %10.16f, %10.16f\n", p2.latitude, p2.longitude, p2.altitude)

	return Point{Lat: p1.latitude, Lng: p1.longitude},
		Point{Lat: p2.latitude, Lng: p2.longitude}
}

func MakePoint(lat, lng float64) Point {
	var gps Point
	gps.Lat = lat
	gps.Lng = lng

	return gps
}

type DegreePoint struct {
	Degree, Minute, Second float64 // 度分秒
}

func (deg *DegreePoint) MakeDegree(degree float64) {
	d, f, m := MakeDegMinSec(degree)
	//s := fmt.Sprintf("%3.0f°%2.0f′%3.14f″", d, f, m)
	deg.Degree = d
	deg.Minute = f
	deg.Second = m
}

func (deg *DegreePoint) ToString() string {
	return fmt.Sprintf("%3.0f°%2.0f′%3.14f″", deg.Degree, deg.Minute, deg.Second)
}

// 经纬度（角度）转化为弧度
func Rad(d float64) float64 {
	return d * math.Pi / 180.0
}

func DegreeFormat(degree float64) string {
	d, f, m := MakeDegMinSec(degree)
	s := fmt.Sprintf("%3.0f°%2.0f′%3.14f″", d, f, m)
	return s
}

// 纬度一秒的距离
func DegreeDistanceLat(lat1, lat2 float64) float64 {
	x := 111319.55 / 3600
	return (lat2 - lat1) * x
}

// 经度一秒的距离
func DegreeDistanceLng(lng1, lng2, lat float64) float64 {
	x := 111319.55 * math.Cos(Rad(lat)) / 3600
	return (lng2 - lng1) * x
}

func GetDistance(p1, p2 Point) float64 {
	radLat1 := Rad(p1.Lat)
	radLat2 := Rad(p2.Lat)
	a := radLat1 - radLat2
	b := Rad(p1.Lng) - Rad(p2.Lng)
	s := 2 * math.Asin(math.Sqrt(math.Pow(math.Sin(a/2), 2)+math.Cos(radLat1)*math.Cos(radLat2)*math.Pow(math.Sin(b/2), 2)))
	s = s * EarthRadius * 1000 // 单位米

	return s
}

// 给定经纬度计算度分秒
func MakeDegMinSec(degree float64) (d, f, m float64) {
	d = math.Floor(degree)
	tmp := degree - math.Floor(degree)
	f = math.Floor(tmp * 60)
	m = (tmp*60 - f) * 60
	return d, f, m
}

// degree,minutes,seconds
func DegMinSecToSeconds(d, m, s float64) float64 {
	return d*3600 + m*60 + s
}

// 给定经纬度，转为角度秒
func DegreeToSeconds(degree float64) float64 {
	d, f, m := MakeDegMinSec(degree)
	return DegMinSecToSeconds(d, f, m)
}

// 给定角度秒，转为经纬度
func SecondsToDegree(seconds float64) float64 {
	var degree = seconds / 3600
	return degree
}

// 给定点为中心，求矩形 左上角、右下角坐标
// lat: 纬度
// lng: 经度
// w: 矩形宽度
// h: 矩形高度
// return: p1 左上角 p2 右下角
func MakeSquare(lat, lng, w, h float64) (p1, p2 Point) {
	// 已知p，找 p1,p2
	/*
		   (p1)----------------.
			|        |         |
			|        |         |
			|--------p---------|
			|        |         |
			|        |         |
			'-----------------(p2)
	*/
	// 求经纬度 角度秒数
	x := DegreeToSeconds(lat)
	y := DegreeToSeconds(lng)

	// 计算经纬度1秒的单位距离 米/秒
	dx1 := DegreeDistanceLat(x, x+1)
	dy1 := DegreeDistanceLng(y, y+1, lat)

	// 计算坐标位移

	p1_x := x + (h/2)/dx1
	p1_y := y - (w/2)/dy1
	p2_x := x - (h/2)/dx1
	p2_y := y + (w/2)/dy1

	// 将坐标角度秒转为经纬度
	p1 = Point{Lat: SecondsToDegree(p1_x), Lng: SecondsToDegree(p1_y)}
	p2 = Point{Lat: SecondsToDegree(p2_x), Lng: SecondsToDegree(p2_y)}
	return p1, p2
}

// 29°32′42.3″ --> 29.545083333333334
func DegMinSecToDegree(deg, min, sec float64) float64 {
	s := DegMinSecToSeconds(deg, min, sec)
	d := SecondsToDegree(s)

	return d
}

// 谷歌地球坐标，转世界坐标
// `29°40′18.28〃北
//  106°42′46.12〃东`
// --> 29.6717444444444425,106.7128111111111082
func ParseGEPointStr(pointStr string) Point {
	//fmt.Printf("\n=======pointStr ============\n %s\n", pointStr)
	rg := regexp.MustCompile(RegGEPointStr)
	names := rg.SubexpNames()
	tmp := rg.FindAllStringSubmatch(pointStr, -1)
	//fmt.Printf("math=%+v\n", tmp)
	result := tmp[0]

	data := map[string]float64{}

	// map[DegLat:29 MinLat:40 SecLat:18.28 DegLng:106 MinLng:42 SecLng:46.12]
	for k, v := range result {
		if names[k] == "" {
			continue
		}

		if n, err := strconv.ParseFloat(v, 64); err == nil {
			data[names[k]] = n
			continue
		} else {
			fmt.Println(v)
			panic(err)
		}
	}

	var p Point
	p.Lat = DegMinSecToDegree(data["DegLat"], data["MinLat"], data["SecLat"])
	p.Lng = DegMinSecToDegree(data["DegLng"], data["MinLng"], data["SecLng"])
	return p
}

func GEPointsParse(ss string) []Point {
	var pointArr []Point
	reg := regexp.MustCompile(`\r?\n\r?\n`)
	xx := reg.Split(ss, -1)
	for _, v := range xx {
		if libs.IsEmpty(v) {
			continue
		}
		pointArr = append(pointArr, ParseGEPointStr(v))
	}

	return pointArr
}

// 位置转视图
func PositionToView(ps structs.Position) (pView structs.PositionView) {
	pView.Id = ps.Id // md5
	pView.Type = ps.Type
	pView.Name = ps.Name
	pView.GpsNumber = ps.GpsNumber
	pView.WriteTime = ps.WriteTime
	pView.Longitude = fmt.Sprintf("%.16f", ps.Longitude)
	pView.Latitude = fmt.Sprintf("%.16f", ps.Latitude)
	pView.Altitude = fmt.Sprintf("%.0f", ps.Altitude)

	return pView
}

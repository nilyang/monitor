package utils

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

const MYSQL_DSN string = "web:111111@tcp(localhost:3306)/fire_alarm?charset=utf8&parseTime=True&loc=Local"

func GetDb() gorm.DB {
	db, err := gorm.Open("mysql", MYSQL_DSN)
	if err != nil {
		fmt.Println("[ERROR] Database connect failed: ", err)
	}
	defer db.Close()

	return *db
}

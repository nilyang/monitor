package utils

import (
	"errors"
	"time"

	"gitee.com/nilyang/monitor/structs"
	"github.com/robbert229/jwt"
)

func JwtToken(secret string, payload structs.Payload, allowExpire bool) (string, error) {
	algorithm := jwt.HmacSha256(secret)
	claims := jwt.NewClaim()
	for key, val := range payload {
		claims.Set(key, val)
	}
	token, err := algorithm.Encode(claims)
	if err != nil {
		return "", errors.New("make token fail")
	}

	// 60 分钟有效期
	if allowExpire {
		nbf := time.Now().Add(time.Duration(-1) * time.Hour)
		exp := time.Now().Add(time.Duration(100) * time.Hour)
		claims.SetTime("nbf", nbf)
		claims.SetTime("exp", exp)
	}

	return token, nil
}
func JwtMakeToken(secret string, payload structs.Payload) (string, error) {
	return JwtToken(secret, payload, true)
}
func JwtGetClaims(secret string, token string) (*jwt.Claims, error) {
	algorithm := jwt.HmacSha256(secret)
	claims, err := algorithm.Decode(token)
	if err != nil {
		return nil, errors.New("invalid token")
	}

	return claims, nil
}

func JwtValidate(secret string, token string) bool {
	algorithm := jwt.HmacSha256(secret)
	if algorithm.Validate(token) != nil {
		return false
	} else {
		return true
	}
}

func JsonResult(code int, data interface{}) map[string]interface{} {
	return map[string]interface{}{
		"code": code,
		"data": data,
	}
}

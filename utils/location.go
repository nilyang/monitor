/**
 * 各地图API坐标系统比较与转换
 * WGS84坐标系：即地球坐标系，国际上通用的坐标系。设备一般包含GPS芯片或者北斗芯片获取的经纬度为WGS84地理坐标系,
 * 谷歌地图采用的是WGS84地理坐标系（中国范围除外）
 * GCJ02坐标系：即火星坐标系，是由中国国家测绘局制订的地理信息系统的坐标系统。由WGS84坐标系经加密后的坐标系。
 * 谷歌中国地图和搜搜中国地图采用的是GCJ02地理坐标系 BD09坐标系：即百度坐标系，GCJ02坐标系经加密后的坐标系
 * 搜狗坐标系、图吧坐标系等，估计也是在GCJ02基础上加密而成的。
 */
package utils

import (
	"math"
	"strconv"
)

const BAIDU_LBS_TYPE = "bd09ll"

const (
	pi   = math.Pi
	a    = 6378245.0
	ee   = 0.00669342162296594323
	x_pi = math.Pi * 3000.0 / 180.0
)

type GpsPoint struct {
	Lat float64
	Lng float64
}

func GpsToString(gps GpsPoint) string {
	var lat string = strconv.FormatFloat(gps.Lat, 'f', -1, 24)
	var lon string = strconv.FormatFloat(gps.Lng, 'f', -1, 24)

	return lat + "," + lon
}

func transformLat(lat float64, lon float64) float64 {
	var ret float64 = -100.0 + 2.0*lat + 3.0*lon + 0.2*lon*lon + 0.1*lat*lon + 0.2*math.Sqrt(math.Abs(lat))
	ret += (20.0*math.Sin(6.0*lat*pi) + 20.0*math.Sin(2.0*lat*pi)) * 2.0 / 3.0
	ret += (20.0*math.Sin(lon*pi) + 40.0*math.Sin(lon/3.0*pi)) * 2.0 / 3.0
	ret += (160.0*math.Sin(lon/12.0*pi) + 320*math.Sin(lon*pi/30.0)) * 2.0 / 3.0
	return ret
}

func transformLon(lat float64, lon float64) float64 {
	ret := 300.0 + lat + 2.0*lon + 0.1*lat*lat + 0.1*lat*lon + 0.1*math.Sqrt(math.Abs(lat))
	ret += (20.0*math.Sin(6.0*lat*pi) + 20.0*math.Sin(2.0*lat*pi)) * 2.0 / 3.0
	ret += (20.0*math.Sin(lat*pi) + 40.0*math.Sin(lat/3.0*pi)) * 2.0 / 3.0
	ret += (150.0*math.Sin(lat/12.0*pi) + 300.0*math.Sin(lat/30.0*pi)) * 2.0 / 3.0
	return ret
}

func outOfChina(lat float64, lon float64) bool {
	if lon < 72.004 || lon > 137.8347 {
		return true
	}
	if lat < 0.8293 || lat > 55.8271 {
		return true
	}
	return false
}

func transform(lat float64, lon float64) GpsPoint {
	if outOfChina(lat, lon) {
		return GpsPoint{lat, lon}
	}
	dLat := transformLat(lon-105.0, lat-35.0)
	dLon := transformLon(lon-105.0, lat-35.0)
	radLat := lat / 180.0 * pi
	magic := math.Sin(radLat)
	magic = 1 - ee*magic*magic
	SqrtMagic := math.Sqrt(magic)
	dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * SqrtMagic) * pi)
	dLon = (dLon * 180.0) / (a / SqrtMagic * math.Cos(radLat) * pi)
	mgLat := lat + dLat
	mgLon := lon + dLon
	return GpsPoint{mgLat, mgLon}
}

//----------------------------坐标系转换-----------------------------//

// 84 to 火星坐标系 (GCJ-02)
// World Geodetic System ==> Mars Geodetic System
func Wgs2Gcj(lat float64, lon float64) GpsPoint {
	dLat := transformLat(lon-105.0, lat-35.0)
	dLon := transformLon(lon-105.0, lat-35.0)
	radLat := lat / 180.0 * pi
	magic := math.Sin(radLat)
	magic = 1 - ee*magic*magic
	SqrtMagic := math.Sqrt(magic)
	dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * SqrtMagic) * pi)
	dLon = (dLon * 180.0) / (a / SqrtMagic * math.Cos(radLat) * pi)
	mgLat := lat + dLat
	mgLon := lon + dLon
	return GpsPoint{mgLat, mgLon}
}

// 火星坐标系 (GCJ-02) to 84
func Gcj2Wgs(lat float64, lon float64) GpsPoint {
	gps := transform(lat, lon)
	lontitude := lon*2 - gps.Lng
	latitude := lat*2 - gps.Lat
	return GpsPoint{latitude, lontitude}
}

// 火星坐标系 (GCJ-02) 与百度坐标系 (BD-09) 的转换算法
// 将 GCJ-02 坐标转换成 BD-09 坐标
// 谷歌转百度
func Gcj2Bd(lat float64, lon float64) GpsPoint {
	x := lon
	y := lat
	z := math.Sqrt(x*x+y*y) + 0.00002*math.Sin(y*x_pi)
	theta := math.Atan2(y, x) + 0.000003*math.Cos(x*x_pi)
	bd_lon := z*math.Cos(theta) + 0.0065
	bd_lat := z*math.Sin(theta) + 0.006
	return GpsPoint{bd_lat, bd_lon}
}

// 火星坐标系 (GCJ-02) 与百度坐标系 (BD-09) 的转换算法
// 将 BD-09 坐标转换成GCJ-02 坐标
//
func Bd2Gcj(lat float64, lon float64) GpsPoint {
	x := lon - 0.0065
	y := lat - 0.006
	z := math.Sqrt(x*x+y*y) - 0.00002*math.Sin(y*x_pi)
	theta := math.Atan2(y, x) - 0.000003*math.Cos(x*x_pi)
	gg_lon := z * math.Cos(theta)
	gg_lat := z * math.Sin(theta)
	return GpsPoint{gg_lat, gg_lon}
}

// (BD-09)-->84
// 百度转大地坐标（谷歌）
func Bd2Wgs(bd_lat float64, bd_lon float64) GpsPoint {
	gcj02 := Bd2Gcj(bd_lat, bd_lon)
	map84 := Gcj2Wgs(gcj02.Lat, gcj02.Lng)
	return map84
}

// 大地坐标-->百度坐标
func Wgs2Bd(lat float64, lon float64) GpsPoint {
	wgs2gcj := Wgs2Gcj(lat, lon)
	gcj2bd := Gcj2Bd(wgs2gcj.Lat, wgs2gcj.Lng)
	return gcj2bd
}

package utils

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitee.com/nilyang/monitor/structs"
	"github.com/astaxie/beego/httplib"
	"github.com/tidwall/gjson"
)

func ImageBase64(path string) (string, error) {
	imgFile, err := os.Open(path)
	if err != nil {
		log.Println(err)
		return "", err
	}

	defer imgFile.Close()

	fInfo, _ := imgFile.Stat()
	buf := make([]byte, fInfo.Size())

	fReader := bufio.NewReader(imgFile)
	fReader.Read(buf)

	imgBase64str := base64.StdEncoding.EncodeToString(buf)
	return imgBase64str, nil
}

func StorageGetFile(cmd string) (*structs.StorageFile, error) {
	url := strings.Join([]string{STORAGE_URL, cmd}, "/")
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	//log.Println(string(body))

	if gjson.Valid(string(body)) {
		f := structs.StorageFile{}
		// gjson.Unmarshal(body, &f)
		// gjson.Unmarshal 已经废弃
		json.Unmarshal(body, &f)
		return &f, nil
	} else {
		return nil, errors.New("json 错误")
	}
}

func StoragePutFile(src_path string, url string) (int64, error) {
	fieldname := "file" // form field name
	body_str, err := PostFile(fieldname, src_path, url)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	value := gjson.Get(body_str, "size")
	return value.Int(), nil

}

func PostFile(fieldname string, filename string, targetUrl string) (string, error) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	//关键的一步操作
	fileWriter, err := bodyWriter.CreateFormFile(fieldname, filepath.Base(filename))
	if err != nil {
		log.Println("error writing to buffer")
		return "", err
	}

	//打开文件句柄操作
	fh, err := os.Open(filename)
	if err != nil {
		log.Println("error opening file")
		return "", err
	}
	defer fh.Close()

	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		log.Println(err)
		return "", err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(targetUrl, contentType, bodyBuf)
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer resp.Body.Close()
	resp_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(resp_body), nil
}

func PostFileBeego(formname string, filename string, targetUrl string) (string, error) {
	//log.Println(targetUrl)
	req := httplib.Post(targetUrl).SetTimeout(100*time.Second, 30*time.Second)
	req.PostFile(formname, filename)
	body, err := req.String()
	if err != nil {
		return "", err
	}

	//log.Println(body)
	return body, nil
}

// 云存储
func CloudSave(filename string) (*structs.StorageFile, error) {
	f, err := StorageGetFile("/dir/assign")
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("http://%s/%s", strings.TrimLeft(f.Url, "http://"), f.Fid)

	size, err := StoragePutFile(filename, url)
	if err != nil {
		return nil, err
	}

	f.Size = size
	return f, nil
}

func CallApi(api string) (string, error) {
	body, err := GetBody(api)
	return string(body), err
}

func SendMessage(url string, params map[string]string) (string, error) {
	req := httplib.Post(url).SetTimeout(100*time.Second, 30*time.Second)
	for key, val := range params {
		req.Param(key, val)
	}

	body, err := req.String()

	if err != nil {
		return "", err
	}
	resp := string(body)
	code := gjson.Get(resp, "code").Int()
	data := gjson.Get(resp, "data").String()
	if code == 20000 {
		return "ok", nil
	} else {
		return "", errors.New(data)
	}
}

func SendMessageJson(url string, jsonData interface{}) (string, error) {
	req := httplib.Post(url).SetTimeout(100*time.Second, 30*time.Second)
	req.JSONBody(jsonData)

	body, err := req.String()

	if err != nil {
		return "", err
	}

	resp := string(body)
	code := gjson.Get(resp, "code").Int()
	data := gjson.Get(resp, "data").String()
	if code == 20000 {
		return "ok", nil
	} else {
		return "", errors.New(data)
	}
}

func GetBody(targetUrl string) ([]byte, error) {
	req := httplib.Get(targetUrl).SetTimeout(100*time.Second, 30*time.Second)
	body, err := req.Bytes()
	if err != nil {
		return nil, err
	}

	return body, nil
}

func GetFile(url string, filename string) error {
	req := httplib.Get(url).SetTimeout(100*time.Second, 30*time.Second)
	err := req.ToFile(filename)
	return err
}

func SendJson(url string, jsonData interface{}) (string, error) {
	req := httplib.Post(url).SetTimeout(100*time.Second, 30*time.Second)
	req.JSONBody(jsonData)

	body, err := req.String()

	if err != nil {
		return "", err
	}

	return body, err
}

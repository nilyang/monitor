package utils

import (
	"fmt"
	"testing"
)

func TestDegreeDistanceLng(t *testing.T) {

	lat := DegMinSecToDegree(29, 40, 15)
	lng1 := DegMinSecToDegree(106, 42, 42.89)
	lng2 := DegMinSecToDegree(106, 42, 44.08)

	// 求经纬度 角度秒数
	x := DegreeToSeconds(lat)
	y1 := DegreeToSeconds(lng1)
	y2 := DegreeToSeconds(lng2)

	// 计算经纬度1秒的单位距离 米/秒
	dy1 := DegreeDistanceLng(y1, y1+1, lat)
	dy2 := DegreeDistanceLng(y2, y2+1, lat)

	fmt.Printf("x= %f, y1 = %f, y2=%f\n", x, y1, y2)
	fmt.Printf("dy2- dy1=%f\n", y2-y1)
	diff := y2 - y1
	fmt.Printf("dy1=%f, dy2=%f\n", dy1, dy2)
	fmt.Printf("dy1=%f, dy2=%f\n", dy1*diff, dy2*diff)

	// 结果如下：
	//x= 106815.000000, y1 = 384162.890000, y2=384164.080000
	//dy2- dy1=1.1900000000023283
	//dy1=26.867704, dy2=26.867704
	//dy1=31.972567, dy2=31.972567

	// 跟GE地图上标尺测量的结果基本一致（标尺测试线条参数如下）
	// 地图长度： 31.47米
	// 地面长度：31.83米
	//     方位：89.99 度
}

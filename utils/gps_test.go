package utils

import (
	"fmt"
	"regexp"
	"testing"
)

func TestDegMinSecToSeconds(t *testing.T) {
	// 29°40′18.28〃
	fmt.Println(DegMinSecToDegree(29, 40, 18.28))
	//29°32′42.3″
	fmt.Println(DegMinSecToDegree(29, 32, 42.3))
}

func TestGEPointRegex(t *testing.T) {
	dgpat := `(?P<DegLat>\d+)\s*°\s*(?P<MinLat>\d+)\s*′\s*(?P<SecLat>\d+(?:.\d+))\s*[″〃]\s*北\s*\r?\n(?P<DegLng>\d+)\s*°\s*(?P<MinLng>\d+)\s*′\s*(?P<SecLng>\d+(?:.\d+))\s*[″〃]\s*东`
	rg := regexp.MustCompile(dgpat)
	str := `29°40′18.28〃北
106°42′46.12〃东`
	names := rg.SubexpNames()
	result := rg.FindAllStringSubmatch(str, -1)[0]

	data := map[string]string{}

	// map[DegLat:29 MinLat:40 SecLat:18.28 DegLng:106 MinLng:42 SecLng:46.12]
	for k, v := range result {
		if names[k] == "" {
			continue
		}
		data[names[k]] = v
		fmt.Printf("%d. match= '%s'\tname= '%s'\n", k, v, names[k])
	}

	if rg.MatchString(str) {
		fmt.Println("Yes")
		fmt.Println(names)
		fmt.Println(data)
	} else {
		fmt.Println("No")
	}

}

func TestGEPointToFloatPoint(t *testing.T) {
	str := `29°40′18.28〃北
106°42′46.12〃东`
	p := ParseGEPointStr(str)
	fmt.Println(p.ToString())
}

func TestGEpoints(t *testing.T) {
	fire := `29°40′18.28〃北
106°42′46.12〃东
`
	firePoint := ParseGEPointStr(fire)
	fmt.Println(firePoint.ToString())

	cars1 := `29°40′4.29〃北
106°42′43.33〃东

29°40′5.82〃北
106°42′43.30〃东

29°40′8.2〃北
106°42′43.32〃东

29°40′11.08〃北
106°42′42.83〃东

29°40′13.22〃北
106°42′43.74〃东

29°40′14.79〃北
106°42′43.97〃东
`
	fmt.Println("che1")

	pp1 := GEPointsParse(cars1)

	for k, v := range pp1 {
		fmt.Printf("%d %s\n", k, v.ToString())
	}

	cars2 := `29°40′15.39〃北
106°42′40.98〃东

29°40′14.77〃北
106°42′42.45〃东

29°40′13.28〃北
106°42′42.70〃东

29°40′12.76〃北
106°42′43.48〃东

29°40′13.96〃北
106°42′43.79〃东

29°40′14.56〃北
106°42′43.28〃东
`
	fmt.Println("che2")
	pp2 := GEPointsParse(cars2)
	for k, v := range pp2 {
		fmt.Printf("%d %s\n", k, v.ToString())
	}
	people := `29°40′14.41〃北
106°42′43.21〃东

29°40′14.35〃北
106°42′42.99〃东

29°40′14.57〃北
106°42′42.95〃东

29°40′14.91〃北
106°42′42.86〃东

29°40′15.29〃北
106°42′42.78〃东

29°40′15.74〃北
106°42′42.64〃东

29°40′16.04〃北
106°42′42.53〃东

29°40′16.46〃北
106°42′42.46〃东

29°40′16.82〃北
106°42′42.50〃东

29°40′16.95〃北
106°42′42.79〃东

29°40′17.05〃北
106°42′43.18〃东

29°40′17.19〃北
106°42′43.49〃东

29°40′17.25〃北
106°42′43.81〃东

29°40′17.45〃北
106°42′44.10〃东

29°40′17.72〃北
106°42′44.25〃东

29°40′18.04〃北
106°42′44.38〃东

29°40′18.44〃北
106°42′44.53〃东

29°40′18.71〃北
106°42′44.79〃东

29°40′18.93〃北
106°42′45.02〃东`

	fmt.Println("ren")
	pp3 := GEPointsParse(people)
	for k, v := range pp3 {
		fmt.Printf("%d %s\n", k, v.ToString())
	}

}

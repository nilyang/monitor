//go:generate goversioninfo -icon=task.ico

package main

import (
	"flag"
	"html"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/mq"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/task/helper"
	"gitee.com/nilyang/monitor/utils"

	"github.com/astaxie/beego/httplib"
)

// 任务生成器，监听MQ，生成任务
// 1. 获取灾情事件，坐标信息
// 2. 将任务推送到任务数据库
var (
	config_path string
	args        []string
	config      libs.ConfigStruct
)

func init() {
	flag.StringVar(&config_path, "c", "./config.json", "-c=config_path")
	str := flag.String("a", "", `-a="args1 args2"`)
	flag.Parse()
	args = strings.Split(*str, " ")

	config = libs.ReadConfig(config_path)

	config.Msgapi = config.SrvApi + "/message/send"
	config.Fileapi = config.SrvApi + "/file"
}

func saveData(token structs.MsgToken) {
	var (
		name     string = token.Name
		location string = token.Location
		picUrl   string = token.PicUrl
	)

	var dir libs.WatchDir
	for i := 0; i < len(config.WatchDirs); i++ {
		if dir = config.WatchDirs[i]; dir.IsZai() {
			break
		}
	}

	//locationFile := dir.Dir + libs.PATH_SEPERRATOR + name + ".txt"
	//imgFile := dir.Dir + libs.PATH_SEPERRATOR + name + ".png"
	//
	//resp := httplib.Get(picUrl)
	//if err := resp.ToFile(imgFile); err != nil {
	//	log.Println(err)
	//	return
	//}

	//if err := ioutil.WriteFile(locationFile, []byte(location), os.ModePerm); err != nil {
	//	log.Println(err)
	//	return
	//}

	// 通知火情发生
	alarmMsg := map[string]string{"msg": html.EscapeString("设备[" + name + "]监测测到灾情发生"), "location": location, "pic": picUrl}
	ret, err := utils.SendMessage(config.Msgapi, alarmMsg)
	if err != nil {
		log.Println(err)
		return
	}

	var urlAlarm = config.SrvApi + "/alarm"
	if err = helper.ReportAlarm(urlAlarm, token); err != nil {
		log.Println(err)
		return
	}

	log.Printf("收到设备[%s]灾情警报,位置:%s\n", name, location)
	log.Println(ret)
}

func savePosition(token structs.MsgToken) {
	var fromTypeName string
	if token.Type == "ren" {
		fromTypeName = "人"
	} else if token.Type == "che" {
		fromTypeName = "车"
	} else {
		log.Println("from type not defined!")
		return
	}

	// 通知在线位置
	var reportUrl = config.SrvApi + "/report"
	if err := helper.ReportRenche(reportUrl, token); err != nil {
		log.Println(err)
		return
	}

	log.Printf("收到%s位置信息: %s !\n", fromTypeName, token.Location)

}

//----- 写文件方式 demo ,暂时不用，考虑是否保留------//

func saveDataDemo(token structs.MsgToken) {
	var (
		//name string = token.Name
		location string = token.Location
		picUrl   string = token.PicUrl
	)

	baseDir := config.Demo.BaseDir

	locationFile := baseDir + libs.PATH_SEPERRATOR + config.Demo.AlarmFile
	imgFile := baseDir + libs.PATH_SEPERRATOR + config.Demo.AlarmPic

	// URL 转
	urlObj, err := url.Parse(picUrl)
	if err != nil {
		log.Println(err)
		return
	}

	cloudHost := config.CloudHost

	pic_uri := urlObj.RequestURI()
	pic_url := urlObj.Scheme + "://" + cloudHost + ":" + urlObj.Port() + pic_uri

	//log.Println("picUrl  =", picUrl)
	//log.Println("pic_url =", pic_url )

	resp := httplib.Get(pic_url)
	if err := resp.ToFile(imgFile); err != nil {
		log.Println(err)
		return
	}

	log.Println(locationFile)
	log.Println(imgFile)
	if err := ioutil.WriteFile(locationFile, []byte(location), os.ModePerm); err != nil {
		log.Println(err)
		return
	}

	// 通知火情发生
	alarmMsg := map[string]string{"msg": html.EscapeString(token.Name + ": 灾情发生"), "location": location, "pic": picUrl}
	ret, err := utils.SendMessage(config.Msgapi, alarmMsg)
	if err != nil {
		log.Println(err)
		return
	}

	var urlAlarm = config.SrvApi + "/alarm"
	if err = helper.ReportAlarm(urlAlarm, token); err != nil {
		log.Println(err)
		return
	}
	log.Println("收到灾情警报!")
	log.Println(ret)
}

func savePositionDemo(token structs.MsgToken) {

	//token.Name // 名字
	//token.Code // 设备编号
	//token.Location // gps位置
	location := []byte(token.Location)
	locationFile := config.Demo.BaseDir + libs.PATH_SEPERRATOR
	var fromTypeName string
	if token.Type == "ren" {
		locationFile += config.Demo.RenFile
		fromTypeName = "人"
	} else if token.Type == "che" {
		fromTypeName = "车"
		locationFile += config.Demo.CheFile
	} else {
		log.Println("from type not defined!")
		return
	}

	if err := ioutil.WriteFile(locationFile, location, os.ModePerm); err != nil {
		log.Println(err)
		return
	}

	// 通知在线位置
	var reportUrl = config.SrvApi + "/report"
	if err := helper.ReportRenche(reportUrl, token); err != nil {
		log.Println(err)
		return
	}

	log.Printf("收到%s位置信息!\n", fromTypeName)
}

func main() {
	mqErr := make(chan error)
	for {
		go monitor(mqErr)

		select {
		case <-mqErr:
			log.Println("遇到错误: ", mqErr)
		}
	}
}

func monitor(mqErr chan<- error) {
	log.Println("task monitor start")

	err := mq.SubscribeMQ(config.Mqdsn, mq.MonitorRoutingKey("*"), func(bytes []byte) {
		// 解码
		token := structs.MsgToken{Message: string(bytes)}
		//log.Printf("token.message = %s\n", token.Message)
		token.ParseToken()
		//if config.Demo.IsDemo {
		//	//if token.Type == "zai" {
		//	//	saveDataDemo(token)
		//	//} else {
		//	//	savePositionDemo(token)
		//	//}
		//} else {
		if token.Type == "zai" {
			saveData(token)
		} else {
			savePosition(token)
		}
		//}
	}, true)

	if err != nil {
		log.Printf("mq subscrbe error : %v\n", err)
	}

	mqErr <- err
}

package helper

import (
	"log"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"
)

// 持久化
// 警报位置信息持久化
func ReportAlarm(urlAlarm string, token structs.MsgToken) error {
	var alarm Alarm
	alarm.Type = token.Type
	alarm.Source = token.Source
	alarm.Code = token.Code
	alarm.Name = token.Name
	t, err := libs.TimeFormatFromT(token.Time)
	if err != nil {
		return err
	}

	alarm.Time = t
	alarm.Point = token.Point
	alarm.Location = token.Location
	alarm.PicUrl = token.PicUrl
	alarm.PicHash = token.Hash
	alarm.JsonData = token.JsonData

	_, err = utils.SendMessageJson(urlAlarm, alarm)

	if err != nil {
		log.Println(err)
	} else {
		log.Println("report ok")
	}

	return err
}

// 人车位置持久化
func ReportRenche(reportUrl string, token structs.MsgToken) error {
	var gps GpsReport
	gps.Type = token.Type
	gps.Source = token.Source
	gps.Code = token.Code
	gps.Name = token.Name
	t, err := libs.TimeFormatFromT(token.Time)
	if err != nil {
		return err
	}
	gps.Time = t
	gps.Point = token.Point
	gps.Location = token.Location
	gps.PicUrl = token.PicUrl
	gps.PicHash = token.Hash
	gps.JsonData = token.JsonData
	_, err = utils.SendMessageJson(reportUrl, gps)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("report ok")
	}

	return err
}

package helper

import "time"

// 灾情警报位置记录
type Alarm struct {
	Id       int64     `json:"id"`
	Type     string    `json:"type"`     // zai|ren|che
	Source   string    `json:"source"`   // moniter|watcher
	Code     string    `json:"code"`     // 设备编号(编码)
	Name     string    `json:"name"`     // 设备名(云台名)
	Time     time.Time `json:"time"`     // refresh|occur time
	Point    string    `json:"point"`    // 点
	Location string    `json:"location"` // 点|矩形对角位置...等...
	PicUrl   string    `json:"pic_url"`
	PicHash  string    `json:"pic_hash"`
	Status   int       `json:"status"`
	JsonData string    `json:"json_data"` // 原始信息 JSON

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// gps 位置记录
type GpsReport struct {
	Type     string    `json:"type"`     // zai|ren|che
	Source   string    `json:"source"`   // moniter|watcher
	Code     string    `json:"code"`     // 设备编号(编码)
	Name     string    `json:"name"`     // 人名、设备名
	Time     time.Time `json:"time"`     // refresh|occur time
	Point    string    `json:"point"`    // 点
	Location string    `json:"location"` // 点|矩形对角位置...等...
	PicUrl   string    `json:"pic_url"`
	PicHash  string    `json:"pic_hash"`
	Status   int       `json:"status"`
	JsonData string    `json:"json_data"` // 原始信息 JSON
}

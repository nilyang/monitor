package helper

import (
	"encoding/json"
	"fmt"
	"log"
	"testing"

	"gitee.com/nilyang/monitor/utils"

	"github.com/tidwall/gjson"
)

func Test(t *testing.T) {
	jsonStr := `{"type":"che","source":"receive","code":"GP-2","name":"救援车-2","time":"2018-03-28T23:54:48Z","point":"106.7121916666666692\r\n29.6706499999999984","location":"106.7121916666666692\r\n29.6706499999999984","pic_url":"","pic_hash":"","status":0,"json_data":"{\"id\":\"8d45c6b371bda72a83a7704df8f42f75\",\"name\":\"救援车-2\",\"type\":2,\"gpsNumber\":\"GP-2\",\"longitude\":106.71219166666667,\"latitude\":29.67065,\"altitude\":0,\"writeTime\":\"2018-03-28T23:54:48\"}"}`
	var gps GpsReport
	json.Unmarshal([]byte(jsonStr), &gps)

	fmt.Printf("json=%s\n", gjson.Get(jsonStr, "json_data").String())
	fmt.Printf("code=%s,time=%+v\njson=%s\n", gps.Code, gps.Time, gps.JsonData)

	reportUrl := "http://localhost:8080/v1/report"
	_, err := utils.SendMessageJson(reportUrl, gps)
	if err != nil {
		log.Println(err)
	} else {
		xx, _ := json.Marshal(gps)
		log.Printf("report ok, xx = %s\n", string(xx))
	}

}

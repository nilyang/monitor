package mq

import (
	"fmt"
	"log"
	"time"

	"gitee.com/nilyang/monitor/utils"
	"github.com/streadway/amqp"
)

const ISLOG = false

const (
	exchangeType = "topic"
	exchangeName = "monitor-exchange"
	queueName    = "monitor_watch_queue"
	consumerTag  = "monitor-consumer"
	dialVhost    = "moniter"
	dialLocale   = "en_US"
)

func MonitorRoutingKey(key string) string {
	return fmt.Sprintf("monitor.watch.%s", key)
}

type Consumer struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	tag     string
	done    chan error
}

func (c *Consumer) Shutdown() error {
	// will close() the deliveries channel
	if err := c.channel.Cancel(c.tag, true); err != nil {
		return fmt.Errorf("Consumer cancel failed: %s", err)
	}

	if err := c.conn.Close(); err != nil {
		return fmt.Errorf("AMQP connection close error: %s", err)
	}

	defer log.Printf("AMQP shutdown OK")

	// wait for handle() to exit
	return <-c.done
}

func Produce(dsn, routingKey string, body []byte) error {
	return produce(dsn, routingKey, body, ISLOG)
}

func produce(dsn, routingKey string, body []byte, isLog bool) error {
	// This function dials, connects, declares, publishes, and tears down,
	// all in one go. In a real service, you probably want to maintain a
	// long-lived connection as state, and publish against that.
	var (
		amqpURI string = dsn
	)

	if ISLOG {
		log.Printf("dialing %q", amqpURI)
	}

	connection, err := amqp.DialConfig(amqpURI, amqp.Config{
		Heartbeat: 10 * time.Second,
		Vhost:     dialVhost,
		Locale:    dialLocale,
	})

	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}
	defer connection.Close()

	if ISLOG {
		log.Printf("got Connection, getting Channel")
	}
	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	if ISLOG {
		log.Printf("got Channel, declaring %q Exchange (%q)", exchangeType, exchangeName)
	}
	if err := channel.ExchangeDeclare(
		exchangeName, // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	if RELIABLE {
		if ISLOG {
			log.Printf("enabling publishing confirms.")
		}
		if err := channel.Confirm(false); err != nil {
			return fmt.Errorf("Channel could not be put into confirm mode: %s", err)
		}

		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer confirmOne(confirms)
	}

	if ISLOG {
		log.Printf("declared Exchange, publishing %dB body (%q)", len(body), string(body))
	}
	if err = channel.Publish(
		exchangeName, // publish to an exchange
		routingKey,   // routing to 0 or more queues
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            body,
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}

func Consume(dsn, bindingKey string, callback func([]byte)) (*Consumer, error) {
	return consume(dsn, bindingKey, callback, ISLOG)
}

func consume(dsn, bindingKey string, callback func([]byte), isLog bool) (*Consumer, error) {
	var (
		amqpURI string = dsn
		ctag    string = consumerTag
	)

	c := &Consumer{
		conn:    nil,
		channel: nil,
		tag:     ctag,
		done:    make(chan error),
	}
	defer c.Shutdown()
	var err error

	if isLog {
		log.Printf("dialing %q", amqpURI)
	}
	c.conn, err = amqp.DialConfig(amqpURI, amqp.Config{
		Heartbeat: 10 * time.Second,
		Vhost:     dialVhost,
		Locale:    dialLocale,
	})
	if err != nil {
		return nil, fmt.Errorf("Dial: %s", err)
	}

	go func() {
		fmt.Printf("closing: %s", <-c.conn.NotifyClose(make(chan *amqp.Error)))
	}()

	if isLog {
		log.Printf("got Connection, getting Channel")
	}
	c.channel, err = c.conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("Channel: %s", err)
	}

	if isLog {
		log.Printf("got Channel, declaring Exchange (%q)", exchangeName)
	}
	if err = c.channel.ExchangeDeclare(
		exchangeName, // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return nil, fmt.Errorf("Exchange Declare: %s", err)
	}

	if isLog {
		log.Printf("declared Exchange, declaring Queue %q", queueName)
	}
	queue, err := c.channel.QueueDeclare(
		queueName, // name of the queue
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Declare: %s", err)
	}

	if isLog {
		log.Printf("declared Queue (%q %d messages, %d consumers), binding to Exchange (key %q)",
			queue.Name, queue.Messages, queue.Consumers, bindingKey)
	}

	if err = c.channel.QueueBind(
		queue.Name,   // name of the queue
		bindingKey,   // bindingKey
		exchangeName, // sourceExchange
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return nil, fmt.Errorf("Queue Bind: %s", err)
	}

	if isLog {
		log.Printf("Queue bound to Exchange, starting Consume (consumer tag %q)", c.tag)
	}
	deliveries, err := c.channel.Consume(
		queue.Name, // name
		c.tag,      // consumerTag,
		false,      // noAck
		false,      // exclusive
		false,      // noLocal
		false,      // noWait
		nil,        // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Consume: %s", err)
	}

	forever := make(chan bool)
	go handle(deliveries, c.done, callback)
	<-forever

	return c, nil
}

// One would typically keep a channel of publishings, a sequence number, and a
// set of unacknowledged sequence numbers and loop until the publishing channel
// is closed.
func confirmOne(confirms <-chan amqp.Confirmation) {
	if ISLOG {
		log.Printf("waiting for confirmation of one publishing")
	}

	if confirmed := <-confirms; confirmed.Ack {
		if ISLOG {
			log.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
		}
	} else {
		log.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

func handle(deliveries <-chan amqp.Delivery, done chan error, callback func([]byte)) {
	for d := range deliveries {
		log.Printf(
			"got %dB delivery: [%v]",
			len(d.Body),
			d.DeliveryTag,
		)
		// 消费者自定义处理函数
		callback(d.Body)

		if RELIABLE {
			d.Ack(false)
		}
	}
	log.Printf("handle: deliveries channel closed")
	done <- nil
}

func PublishMQ(dsn, routingKey string, body []byte, isEncode bool) bool {
	if isEncode {
		body = utils.EncryptMsg(string(body))
	}
	err := Produce(dsn, routingKey, body)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

const RELIABLE = true

func SubscribeMQ(dsn, routingKey string, callback func([]byte), isDecode bool) error {
	var cb func([]byte)
	if isDecode {
		cb = func(bytes []byte) {
			callback(utils.DecryptMsg(bytes))
		}
	} else {
		cb = callback
	}

	_, err := Consume(dsn, routingKey, cb)

	return err
}

//go:generate goversioninfo -icon=logo.ico
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/mq"
	"gitee.com/nilyang/monitor/structs"
	"gitee.com/nilyang/monitor/utils"

	"github.com/go-ini/ini"
	"github.com/tidwall/gjson"
)

type uploadResult struct {
	Id   int64  `json:"id"`
	Url  string `json:"url"`
	Hash string `json:"hash"`
	Fid  string `json:"fid"`
}

var (
	AlarmUrl    string // 警报
	PositionUrl string // 人车位置
	FileApi     string
	MqDsn       string = "amqp://moniter:111111@localhost:5672"
	CacheDir    string

	GisWidth        float64 = 150
	GisHeight       float64 = 100
	RefreshInterval         = 5
)

func initConfig() error {
	cfg, err := ini.Load("app.conf")
	if err != nil {
		return err
	}

	section := cfg.Section("api")
	url, err := section.GetKey("alarm")
	if err != nil {
		return err
	}
	AlarmUrl = url.String()
	if AlarmUrl == "" {
		return errors.New("empty alarm url")
	}

	position, err := section.GetKey("position")
	if err != nil {
		return err
	}
	PositionUrl = position.String()
	if PositionUrl == "" {
		return errors.New("empty position url")
	}

	api, err := section.GetKey("fileapi")
	if err != nil {
		return err
	}

	FileApi = api.String()
	if FileApi == "" {
		return errors.New("empty fileapi")
	}

	cache, _ := cfg.Section("cache").GetKey("dir")
	if cache != nil {
		CacheDir = cache.String()
		if CacheDir == "" {
			CacheDir = "cache"
		}
	} else {
		CacheDir = "cache"
	}

	width, _ := cfg.Section("square").GetKey("width")
	if width != nil {
		GisWidth, _ = width.Float64()
	}

	height, _ := cfg.Section("square").GetKey("height")
	if height != nil {
		GisHeight, _ = height.Float64()
	}

	refreshInterval, _ := cfg.Section("refresh").GetKey("interval")
	if height != nil {
		RefreshInterval, _ = refreshInterval.Int()
	}

	return nil
}

func init() {
	err := initConfig()
	log.Printf("w:%f, h:%f", GisWidth, GisHeight)
	if err != nil {
		log.Fatalln("config_error: ", err)
	}
}

func main() {
	ticker := time.NewTicker(time.Duration(RefreshInterval) * time.Second)
	for {
		alarmCheckLoop()
		deviceGpsCheckLoop()
		<-ticker.C
	}
}

func alarmCheckLoop() {
	alarms := alarmCheck()
	if libs.IsEmpty(alarms) {
		return
	}
	l := len(alarms)
	//  去重报警
	for i := 0; i < l; i++ {
		alarmProcess(alarms[i])
	}
}

// 消息处理
// 1. 下载 images
// 2. 上传 cloud
// 3. 组装消息
// 4. 发送 MQ
func alarmProcess(alarm structs.Alarm) {
	var msg string

	if len(alarm.Images) == 0 {
		log.Println("ERROR: alarms no images")
		return
	}

	// 下载最第一张图片
	var alarmImgUrl, picCloudUrl, picCloudHash string

	alarmImgUrl = alarm.Images[0]

	filename := alarm.StationId + "." + filepath.Base(alarmImgUrl)
	if err := alarmDownloadRemoteImage(alarmImgUrl, filename); err != nil {
		log.Println(err)
		return
	}

	respImage, err := alarmSaveImagesCloud(filename)
	if err != nil {
		log.Println(err)
		return
	}

	picCloudUrl = respImage.Url
	picCloudHash = respImage.Hash

	//fmt.Printf("respImage = %+v\n", respImage)

	code := alarm.StationId // 基站编码
	name := alarm.Name      // 基站名字
	// \r\n 兼容Windows txt
	// 暂时将火情报警位置设为为 观察点到火点之间的矩形
	// 地理位置矩形计算
	p1, p2 := utils.MakeSquare(alarm.FireLatitude, alarm.FireLongitude, GisWidth, GisHeight)
	//log.Println(p1,p2)
	//{106.60875157287869 29.922112375006357}
	//{106.60771509378796 29.923459847215867}
	location := fmt.Sprintf("%4.16f\r\n%4.16f\r\n%4.16f\r\n%4.16f",
		p1.Lat, p2.Lat,
		p2.Lng, p1.Lng)
	alarmInfo, err := json.Marshal(alarm)
	if err != nil {
		log.Println(err)
		return
	}

	point := fmt.Sprintf("%4.16f\r\n%4.16f", alarm.FireLongitude, alarm.FireLatitude)

	nowtime := time.Now().Format("2006-01-02T15:04:05")
	alarmJson := string(alarmInfo)
	msg = fmt.Sprintf("zai|receive|%s|%s|%s|%s|%s|%s|%s|%s",
		code, name, nowtime, point, location, picCloudUrl, picCloudHash, alarmJson)
	// type|event|code|name|time|point|location|picCloudUrl|picCloudHash|jsondata

	if mq.PublishMQ(MqDsn, mq.MonitorRoutingKey("watcher"), []byte(msg), true) {
		log.Println("PublishMQ: Alarm OK")
	} else {
		log.Println("PublishMQ: Alarm FAIL")
	}

}

func alarmCheck() []structs.Alarm {
	body, err := utils.GetBody(AlarmUrl)
	if err != nil {
		log.Println(err)
		return nil
	}

	if !gjson.Valid(string(body)) {
		log.Println("Invalide json string")
		return nil
	}

	var xArray []structs.Alarm
	json.Unmarshal(body, &xArray)
	//log.Printf("alarm xArray = %v\n", xArray)

	//for _,x := range xArray {
	//	log.Printf("name:%s, stationId:%s\n", x.Name, x.StationId)
	//	log.Printf("sLng:%3.16f, sLat:%3.16f", x.StationLongitude, x.StationLatitude)
	//	log.Printf("flng:%3.16f, fLat:%3.16f", x.StationLongitude, x.FireLatitude)
	//}
	return xArray
}

func alarmDownloadRemoteImage(imgurl string, filename string) error {
	tmpFile := CacheDir + string(os.PathSeparator) + filename
	if _, err := os.Stat(CacheDir); os.IsNotExist(err) {
		if err := os.Mkdir(CacheDir, 0755); err != nil {
			log.Println("make cache dir fail")
			return err
		}
	}

	err := utils.GetFile(imgurl, tmpFile)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func alarmSaveImagesCloud(filename string) (*uploadResult, error) {
	tmpFile := CacheDir + string(os.PathSeparator) + filename
	retBody, err := utils.PostFile("image", tmpFile, FileApi)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if gjson.Valid(retBody) == false {
		return nil, errors.New("the response is invalid json string")
	}

	code := gjson.Get(retBody, "code")
	if code.Int() == 50000 {
		log.Println(gjson.Get(retBody, "data").String())
		return nil, errors.New("upload fail")
	}

	var result uploadResult
	result.Url = gjson.Get(retBody, "data.url").String()
	result.Id = gjson.Get(retBody, "data.id").Int()        // Int
	result.Hash = gjson.Get(retBody, "data.hash").String() // Int
	result.Fid = gjson.Get(retBody, "data.fid").String()   // Int

	return &result, nil
}

//------------人车监控----------------//

func deviceGpsCheck() []structs.Position {
	body, err := utils.GetBody(PositionUrl)
	if err != nil {
		log.Println(err)
		return nil
	}

	if !gjson.Valid(string(body)) {
		log.Println("Invalide json string")
		return nil
	}

	var xArray []structs.Position
	json.Unmarshal(body, &xArray)

	//log.Printf("devices xArray = %v\n", xArray)
	//for _,x := range xArray {
	//	log.Printf("Type:%d, Id:%s, Name:%s\n, ", x.Type, x.Id, x.Name)
	//	log.Printf("Lng:%3.16f, Lat:%3.16f", x.Longitude, x.Latitude)
	//	log.Printf("gpsNumber: %s, writeTime: %s", x.GpsNumber, x.WriteTime)
	//}
	return xArray
}

func deviceGpsProcess(position structs.Position) {
	alarmJson, err := json.Marshal(position)
	if err != nil {
		log.Println(err)
		return
	}
	var pType string
	if position.Type == 1 {
		pType = "ren"
	} else {
		pType = "che"
	}
	location := fmt.Sprintf("%4.16f\r\n%4.16f", position.Longitude, position.Latitude)
	point := location

	msg := fmt.Sprintf("%s|receive|%s|%s|%s|%s|%s|%s|%s|%s",
		pType, position.GpsNumber, position.Name, position.WriteTime, point, location, "", "", alarmJson)
	// type|event|code|name|time|point|location|picid|picurl|pichash|jsondata

	if mq.PublishMQ(MqDsn, mq.MonitorRoutingKey(pType), []byte(msg), true) {
		log.Printf("PublishMQ Position: OK\n")
	} else {
		log.Println("PublishMQ Position: FAIL")
	}
}

func deviceGpsCheckLoop() {
	positions := deviceGpsCheck()
	if libs.IsEmpty(positions) {
		return
	}
	l := len(positions)
	//  去重报警
	for i := 0; i < l; i++ {
		deviceGpsProcess(positions[i])
	}

}

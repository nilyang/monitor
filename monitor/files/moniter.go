//go:generate goversioninfo -icon=icon.ico

package main

import (
	"flag"
	"fmt"
	"log"
	"runtime"
	"strings"
	"sync"
	"time"

	"gitee.com/nilyang/monitor/libs"
	"gitee.com/nilyang/monitor/mq"
	"gitee.com/nilyang/monitor/utils"
	"github.com/fsnotify/fsnotify"
	"github.com/tidwall/gjson"
)

var (
	config_path  string
	args         []string
	rabbitmq_dsn string
	fileapi      string
)

func init() {
	flag.StringVar(&config_path, "c", "./config.json", "--c=config_path")
	str := flag.String("a", "", `-a="args1 args2"`)
	flag.Parse()
	args = strings.Split(*str, " ")
}

func spinner(delay time.Duration) {
	for {
		for _, r := range `-\|/` {
			fmt.Printf("\r%c", r)
			time.Sleep(delay)
		}
	}
}

func main() {
	exit := make(chan bool)

	watchers, err := makeWatchers()
	if err != nil {
		log.Println(err.Error())
	}

	log.Printf("系统信息 CPU 数量:  【%d】\n", runtime.NumCPU())

	for i, watch_map := range watchers {
		log.Printf("开启线程 [%d] 监控 【%s】，路径【%s】\n", i, watch_map.WatchDir.Type, watch_map.WatchDir.Dir)
		go dirMonitor(watch_map)
	}

	<-exit
	runtime.Goexit()
}

func makeWatchers() ([]libs.WatchMap, error) {
	var watchers []libs.WatchMap

	config := libs.ReadConfig(config_path)
	rabbitmq_dsn = config.Mqdsn
	fileapi = config.Fileapi
	watch_len := len(config.WatchDirs)

	for i := 0; i < watch_len; i++ {
		watch_dir := config.WatchDirs[i]
		Watch, err := fsnotify.NewWatcher()
		if err != nil {
			log.Println("初始化监控错误: ", err.Error())
			return watchers, err
		}

		if err := Watch.Add(watch_dir.Dir); err != nil {
			log.Println("添加监控路径错误: ", watch_dir.Dir)
			return watchers, err
		}
		watchers = append(watchers, libs.WatchMap{WatchDir: watch_dir, Watch: *Watch})
	}
	return watchers, nil
}

// 拷贝文件到文件系统
func dirMonitor(watch_map libs.WatchMap) {
	Watch := watch_map.Watch
	WatchDir := watch_map.WatchDir

	defer Watch.Close()
	var (
		lock        sync.Mutex
		msgIsLocked bool = false
	)

	for {
		select {
		case event := <-Watch.Events:
			if !msgIsLocked {
				msgIsLocked = true
				lock.Lock()
				sendMessage(event, WatchDir)
				//libs.FileGetContents(event.Name)
				lock.Unlock()
				msgIsLocked = false
			}
		case err := <-Watch.Errors:
			log.Println(err)
			return
		}
	}
}

func isWrite(event fsnotify.Event) bool {
	return event.Op&fsnotify.Write == fsnotify.Write
}
func isCreate(event fsnotify.Event) bool {
	return event.Op&fsnotify.Create == fsnotify.Create
}
func isRename(event fsnotify.Event) bool {
	return event.Op&fsnotify.Rename == fsnotify.Rename
}
func isRemove(event fsnotify.Event) bool {
	return event.Op&fsnotify.Remove == fsnotify.Remove
}

func getMessage(event fsnotify.Event, WatchDir libs.WatchDir) (canRemove bool, canCopy bool) {
	canRemove = false
	canCopy = false
	f := libs.MyPath(event.Name)

	if f.IsTxt() {
		if isRemove(event) {
			log.Printf("[" + f.Name() + "] 被移除了")
			canRemove = true
		} else if isCreate(event) {
			color_msg := ""
			if WatchDir.Type == "zai" {
				color_msg = " [灾情] [%s] 发生！"
			} else if WatchDir.Type == "che" {
				color_msg = " [车辆] [%s] 派出！"
			} else if WatchDir.Type == "ren" {
				color_msg = " [人员] [%s] 派出！"
			}

			log.Printf(color_msg, f.PureName())
			canCopy = true
		} else if isWrite(event) {
			color_msg := ""
			if WatchDir.Type == "zai" {
				color_msg = " [灾情] [%s] 坐标改变了！"
			} else if WatchDir.Type == "che" {
				color_msg = " [车辆] [%s] 坐标改变了！"
			} else if WatchDir.Type == "ren" {
				color_msg = " [人员] [%s] 坐标改变了！"
			}
			log.Printf(color_msg, f.PureName())
			canCopy = true
		}
	} else if f.IsPng() {
		if isRemove(event) {
			canCopy = false
			canRemove = true
		} else {
			canCopy = true
		}
	}

	return canCopy, canRemove
}

func sendMessage(event fsnotify.Event, WatchDir libs.WatchDir) {
	f := libs.MyPath(event.Name)
	srcFile := WatchDir.Dir + libs.PATH_SEPERRATOR + f.Name()
	canCopy, canRemove := getMessage(event, WatchDir)

	// 将灾情发送到消息系统，通知任务处理进程创建任务
	if canCopy && WatchDir.IsZai() && f.IsTxt() {
		time.Sleep(time.Millisecond * 100) // 暂停100毫秒等待释放独占权限，以便读取文件内容
		var location string
		gisData, err := libs.FileGetContents(srcFile)
		location = string(gisData)
		if libs.IsEmpty(location) || err != nil {
			log.Printf("[%s][%s]坐标信息读取失败:[%s]", WatchDir.Type, f.PureName(), err)
			return
		} else {
			// 异步操作：将消息发送到MQ
			zaiImgFile := WatchDir.Dir + libs.PATH_SEPERRATOR + f.PureName() + ".png"
			retBody, err := utils.PostFile("image", zaiImgFile, fileapi)
			if err != nil {
				log.Println(err)
				return
			}

			if gjson.Get(retBody, "code").Int() == 50000 {
				log.Println(gjson.Get(retBody, "data").String())
				return
			}

			code := utils.Md5sumString(f.PureName())
			name := f.PureName()
			// id := gjson.Get(retBody, "data.id").Int()
			picUrl := gjson.Get(retBody, "data.url").String()
			hash := gjson.Get(retBody, "data.hash").String()
			location := strings.TrimFunc(string(gisData), func(r rune) bool {
				return r == ' ' || r == '\r' || r == '\n' || r == '\t'
			})
			extraJson := ""
			nowtime := time.Now().Format("2006-01-02T15:04:05")
			msg := fmt.Sprintf("zai|moniter|%s|%s|%s|%s|%s|%s|%s|%s",
				code, name, nowtime, "", location,
				// id,
				picUrl, hash, extraJson)
			mq.PublishMQ(rabbitmq_dsn, mq.MonitorRoutingKey("moniter"), []byte(msg), true)
		}
	}

	if canRemove {
		log.Println("Remove ", f.PureName())
	}
}
